﻿using UnityEngine;
using System.Collections;

public class AnimateMaterialHue : MonoBehaviour {

    public struct HsvColor
    {
        public float h;
        public float s;
        public float v;
    }
    private Material animateMaterial, startMaterial, endMaterial;
    private Color startColor;
    private HsvColor hsvColor;
    public float cycleSeconds = 10.0f;


	// Use this for initialization
	void Start () {
        animateMaterial = GetComponent<MeshRenderer>().material;
        startColor = animateMaterial.color;
        Color.RGBToHSV(startColor, out hsvColor.h, out hsvColor.s, out hsvColor.v);
    }
	
	// Update is called once per frame
	void Update () {
        float hue = (Time.time / cycleSeconds) - Mathf.Floor(Time.time / cycleSeconds);
        // Debug.Log (hue);
        animateMaterial.color = Color.HSVToRGB(hue, hsvColor.s, hsvColor.v, false);
    }

}
