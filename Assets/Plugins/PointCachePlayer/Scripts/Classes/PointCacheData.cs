﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

namespace pc2unity_20 {

	public class PointCacheData {
		Vector3[] arr;
		public int VerticesCount;
		public int FramesCount;

		public Vector3  this [int frame, int vertex] {
			get{
				return arr[frame*VerticesCount + vertex];
			}	

			set{
				arr[frame*VerticesCount + vertex] = value;
			}	
		}

		public PointCacheData (string path){
			FileStream fs = new FileStream(path, FileMode.Open);
			BinaryReader binReader = new BinaryReader(fs);
 
			binReader.ReadChars(12); //signature
			binReader.ReadInt32(); //file version
			VerticesCount = binReader.ReadInt32();
			binReader.ReadSingle(); // start frame
	        binReader.ReadSingle(); //sample rate
			FramesCount = binReader.ReadInt32();

			arr = new Vector3[VerticesCount*FramesCount];
			for (int f = 0; f < FramesCount; f++) {
 				for (int p = 0; p <VerticesCount; p++) {
					Vector3 pos = new Vector3();
					pos.x = binReader.ReadSingle();
					pos.y = binReader.ReadSingle();
					pos.z = binReader.ReadSingle();
					this[f, p] = pos;
				} 
			}

			binReader.Close();
			fs.Close();
		}


		public Vector3[] GetFrame(int idx){
			Vector3[] result = new Vector3[VerticesCount];
			for(int v = 0; v<VerticesCount; v++){
				result [v] = this [idx, v];	
			}
			return result;
		}
	}
}
