﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text.RegularExpressions; 


namespace pc2unity_20{

	[System.Serializable]
	public class Polygon{
		public int SmoothingGroup;
		public Vector3 Normal;
		public List<UMeshVertex> corners = new List<UMeshVertex>();
		public float AO;
 
		public List<Face> AttachedTris = new List<Face>();

		public bool IsContainFace( Face face ){
			int matches = 0;
			for(int c = 0; c<corners.Count; c++ ){
				if(corners[c].Equals( face.Va ) || corners[c].Equals( face.Vb ) || corners[c].Equals( face.Vc )){
					matches ++;
				}
			}
			if(matches == 3){
				AttachedTris.Add(face);
				return true;
			} else {
				return false;
			}
 		}

		public void BuildTriangles( List<int> trisList,  bool flipNormals ){
			if(flipNormals){
				for(int c = 1; c<corners.Count-1; c++){
					trisList.Add(corners[0].ThisIdx);
					trisList.Add(corners[c].ThisIdx);
					trisList.Add(corners[c+1].ThisIdx);
				}
			} else {
				for(int c = 1; c<corners.Count-1; c++){
					trisList.Add(corners[c+1].ThisIdx);
					trisList.Add(corners[c].ThisIdx);
					trisList.Add(corners[0].ThisIdx);
 				}
			}
		}

		public void CalcNormal( float normalSign ){
			if(corners.Count == 3){
				Vector3 ab = (corners[1].Position.Value- corners[0].Position.Value);
				Vector3 ac = (corners[2].Position.Value- corners[0].Position.Value);
				Normal = Vector3.Cross(  ac, ab ) ;

 
			} else {
				Vector3 ab = (corners[2].Position.Value- corners[0].Position.Value);
				Vector3 ac = (corners[1].Position.Value- corners[3].Position.Value);
				Normal = Vector3.Cross(   ab , ac ) ;
			}

		 
			float num = Vector3.Magnitude (Normal);
			if (num > 1E-05) {
				Normal /= num ;
				Normal *= normalSign;
			} else {
				Normal = Vector3.one;	
			}

		}


		public NormalVertex GetNextNormal( NormalVertex nv ){
			int nvIdx = -1;
			for(int c = 0; c<corners.Count; c++){
				if(corners[c].Normal == nv){
					nvIdx = c;
					break;
				}
			}
			return corners[ (nvIdx+1) % corners.Count].Normal;
		}
	 
	}
	 
	public class MapVertex{
		public Vector2 Value;

		public MapVertex( Vector2 uv){
			Value = uv;
		}
	}
	 
	public class PositionVertex{
		
		public Vector3 Value;

		public PositionVertex( Vector3 objPosition ){
			Value = objPosition;	
		} 
	}

	#region SUBMESHES 
	public class SubMesh{
		public string MaterialName;
		public List<Polygon> Polygons = new List<Polygon>(); 

		public SubMesh (string materialName){
			MaterialName = materialName;
		}


		public void AddPolygon( Polygon p ){
			Polygons.Add(p);
		}
	}

	public class SubMeshesList{
		List<SubMesh> items = new List<SubMesh>();

	 
		public SubMesh Current = new SubMesh("Default submesh");

		public void SetCurrent(string materialName){
	 		for(int s = 0; s<items.Count; s++){
				if(items[s].MaterialName == materialName){
					Current = items[s];
					return;
				}  
			}
			SubMesh newItem =new SubMesh(materialName);
			items.Add(newItem);

			Current = newItem;
			//Debug.LogFormat("Setted new sub mesh: {0}", Current.MaterialName);
		}

		public int Count{
			get{
				if(items.Count == 0){
					return 1;
				} else {
					return items.Count;
				}
			}
		}

		public SubMesh this [int idx]{
			get{
				if(items.Count == 0){
					return Current;
				} else {
					return items[idx];
				}
			}
		}

		public void PrintStatistic(){
			string result = string.Format("submeshes count: {0} names: ", this.Count );
			for(int s = 0; s<this.Count; s++ ){
				result +=  this[s].MaterialName + ", ";
			}
			Debug.Log(result);
	 	}

		public void AddPolygon(ObjData obj, RawPolygon rawPolygon  ){
	 		if(Current == null){
				Current = new SubMesh("Default");
			} 

			Polygon p = new Polygon();

			p.SmoothingGroup = rawPolygon.SmoothingGroup;
			for(int c = 0; c<rawPolygon.Count; c++){
				PositionVertex pv = obj.Vertices[rawPolygon[c].PosVertIdx];
				MapVertex mv  =  rawPolygon[c].MapVertIdx == -1? null: obj.MapVertices[rawPolygon[c].MapVertIdx];
				NormalVertex nv = obj.Normals.Search(pv, p);
				p.corners.Add ( obj.UnityVertices.Search( pv, mv, nv ) );
			}
			//Debug.LogFormat ("rawPolygon  = [{0}] ParentSubmesh = [{1}] p = [{2}]", rawPolygon, rawPolygon.ParentSubmesh==null, p);
			rawPolygon.ParentSubmesh.AddPolygon( p ); 

			obj.AllPolygons.Add( p );
		}
	 
	}
	#endregion

	#region RAWPOLYGON
	public class RawPolygon{
		List<RawPolygonCorner> corners = new List<RawPolygonCorner>();
		public int SmoothingGroup;
		public SubMesh ParentSubmesh;

		public RawPolygon(int smoothingGroup, SubMesh parentSubMesh){
			SmoothingGroup = smoothingGroup;
			ParentSubmesh = parentSubMesh;
		}

		public int Count{
			get{
				return corners.Count;
			}
		}

		public void AddCorner(int posVertIdx, int mapVertIdx ){
			corners.Add(new RawPolygonCorner(posVertIdx, mapVertIdx ));
		}


		public RawPolygonCorner this [int idx]{
			get{
				return corners[idx];
			}
		}
	}


	public struct RawPolygonCorner{
	 
		public int PosVertIdx;
		public int MapVertIdx;

		public RawPolygonCorner (int posVertIdx, int mapVertIdx ){
			PosVertIdx = posVertIdx;
			MapVertIdx = mapVertIdx;
		}

		public void PrintInfo(){
			Debug.LogFormat( "PosVertIdx: {0} MapVertIdx:{1}", PosVertIdx, MapVertIdx);
		}
	}
	#endregion

	#region UMESHVERTICES
	public struct UMeshVertex{
		public int ThisIdx;
		public PositionVertex Position;
		public NormalVertex Normal;
		public MapVertex UV; 

		public override bool Equals (object obj){
			UMeshVertex other = (UMeshVertex)obj;
			return  Position == other.Position && Normal == other.Normal && UV == other.UV;
		}

		public override int GetHashCode (){
			if(UV==null){
				return Position.GetHashCode()+Normal.GetHashCode();
			} else {
				return Position.GetHashCode()+Normal.GetHashCode()+ UV.GetHashCode(); 
			}
		}

 
	}


	public class UMeshVerticesList{
		Dictionary< UMeshVertex, int> itemsDict = new Dictionary< UMeshVertex, int>();
		List <UMeshVertex> items = new List<UMeshVertex>();

		public int Count{
			get{
				return items.Count;
			}
		}

		public UMeshVertex this[int idx]{
			get{
				return items[idx];
			}
		}

	 	public UMeshVertex Search( PositionVertex position, MapVertex mapVertex, NormalVertex normalVertex  ){
			UMeshVertex newItem = new UMeshVertex();
			int idx = items.Count;
			newItem.ThisIdx = items.Count;
			newItem.Position = position;
			newItem.Normal = normalVertex;
			newItem.UV = mapVertex;

			int findedIdx = -1;
			if( itemsDict.TryGetValue(newItem, out findedIdx) ){
				return items[findedIdx];
			} else {
				itemsDict.Add(newItem, idx);
	 			items.Add( newItem );	
				return newItem;
			}
		}

	//	public void Finalize(){
	//		
	//	}		
	}

	#endregion

	#region NORMALS
	public class NormalVertex{
		public Vector3 Value;
 
 
 		public float AO;
		public int SmoothingGroup;
		public PositionVertex Position;
		public List<Polygon> AdjacentPolygons;
 		public List<NormalVertex> AdjacentNormals;

		public NormalVertex (int smoothingGroup, PositionVertex posVert){
			Value = Vector3.zero;
			SmoothingGroup = smoothingGroup;
			Position = posVert;
			AdjacentPolygons = new List<Polygon>();
			AdjacentNormals = new List<NormalVertex>();
		}
	 
		public void AddPolygonIfUnique( Polygon p ){
			if(AdjacentPolygons.IndexOf(p)>0){
				return;
			} else {
				AdjacentPolygons.Add( p );
			}
		}

		public void FillAdjacentNormals(){
			for(int p = 0; p<AdjacentPolygons.Count; p++){
				AdjacentNormals.Add(AdjacentPolygons[p].GetNextNormal(this));
			}
		}

		public void CalcNormal(){
 
	 		Value = Vector3.zero;
			for(int i = 0; i<AdjacentPolygons.Count; i++){
				Value += AdjacentPolygons[i].Normal ;
			}

			float num = Vector3.Magnitude (Value);
			if (num > 1E-05) {
				Value /= num;
			} else {
				for(int e = 0; e<AdjacentNormals.Count; e++){
					Value +=  (   Position.Value - AdjacentNormals[e].Position.Value) ;
				}
				Value.Normalize();
			}
 
		}

		public override bool Equals (object obj){
			 NormalVertex other = (NormalVertex)obj;
			 if(other == null){
			 	return false;
			 }
			 return SmoothingGroup == other.SmoothingGroup && Position == other.Position;
		}

		public override int GetHashCode (){
			return Position.GetHashCode() ^ SmoothingGroup ;
		}
	}

	public class NormalsList{
		List<NormalVertex> items = new List<NormalVertex>();
		Dictionary<NormalVertex, int> itemsDict = new Dictionary<NormalVertex, int>();
	 

		public NormalVertex Search ( PositionVertex posVertex, Polygon polygon ){
			NormalVertex newNormal = new NormalVertex(polygon.SmoothingGroup, posVertex);
			int findedIdx = -1;

			if( itemsDict.TryGetValue(newNormal, out findedIdx)){
				items[findedIdx].AddPolygonIfUnique( polygon );
				return items[findedIdx];
			}
			 
	 		newNormal.AddPolygonIfUnique(polygon);
			itemsDict.Add(newNormal, items.Count);
			items.Add(newNormal);
			//Debug.LogFormat("added normal with sg: {0}", newNormal.SmoothingGroup);
			return newNormal;
		}


		public int Count{
			get{
				return items.Count;
			}
		}

		public NormalVertex this [int idx]{
			get{
				return items[idx];
			}
		}

		public void RecalculateAllNormals( ){
			for(int i = 0; i<items.Count; i++){
				items[i].CalcNormal();
			}
		}
	}

	#endregion

	public class Face{
		public UMeshVertex Va;
		public UMeshVertex Vb;
		public UMeshVertex Vc;
		public Polygon ParentPolygon;
	}

	public class ObjData {
 
		public Color[] debugColors = new Color[4] {Color.red, Color.green, Color.blue, Color.yellow}; 

		public SubMeshesList SubMeshes = new SubMeshesList();	
		public List<PositionVertex> Vertices = new List<PositionVertex>();
		public NormalsList Normals = new NormalsList();  
		public List<MapVertex> MapVertices = new List<MapVertex>();  
		public UMeshVerticesList UnityVertices = new UMeshVerticesList();
		public List <Polygon> AllPolygons = new List<Polygon>();
		List<RawPolygon> tempRawPolygonsList;
//		public int[] AllTris;
		public List<Face> Faces = new List<Face>();


		char[] spaceSeparator = @" ".ToCharArray();
 

		public Mesh refMesh;
		string[] subString;
		bool hasUvData;

		Vector3 PivotOffset;
		SmoothingGroupImportModeEnum SGMode;
		float ScaleFactor;
		bool FlipNormals;
		bool SwapYZ; 


 

		public ObjData( string path,  Vector3 pivotOffset, SmoothingGroupImportModeEnum sGMode, float scaleFactor, bool flipNormals, bool swapZY ){
			PivotOffset = pivotOffset;
			SGMode = sGMode;
			ScaleFactor = scaleFactor;
			FlipNormals = flipNormals;
			SwapYZ = swapZY;

			System.IO.TextReader objFile = System.IO.File.OpenText(path);
			int currentSmoothingGroup = 0;
	 		hasUvData = false;
			tempRawPolygonsList = new List<RawPolygon>();

	 		//System.Diagnostics.Stopwatch sw = System.Diagnostics.Stopwatch.StartNew();
	 		bool offSGMode = false;
	 		int offSGcounter = 1;

			while (true){
				string str = objFile.ReadLine();
				if(str == null) break;
				if(str.StartsWith("vt ")){
					subString = str.Split( spaceSeparator, System.StringSplitOptions.RemoveEmptyEntries );
					MapVertices.Add( new MapVertex( new Vector2(subString[1].ToFloat(), subString[2].ToFloat())));
					hasUvData = true;
					continue;
				} else if(str.StartsWith("v ")){
					subString = str.Split( spaceSeparator, System.StringSplitOptions.RemoveEmptyEntries );
					Vector3 defaultPos = Vector3.zero;
					if(SwapYZ){
						defaultPos = new Vector3(subString[1].ToFloat(), subString[3].ToFloat(), subString[2].ToFloat() ) ; 
					} else {
						defaultPos = new Vector3(subString[1].ToFloat(), subString[2].ToFloat(), subString[3].ToFloat() ) ;  
					}
					Vertices.Add(new PositionVertex( defaultPos ) ); 
					continue;
				} 	else if (str.StartsWith("usemtl")){
					SubMeshes.SetCurrent(str.Remove(0,7));
					continue;
				}  else if( str.StartsWith ("s off")){
					offSGMode = true;
					continue;
				} else if ( str.StartsWith ("s " )){ 
					offSGMode = false;
					currentSmoothingGroup = (str.Remove(0,2)).ToInt();
					continue;
				} else if (str.StartsWith("f ")){
					int sg =  currentSmoothingGroup;
					if(SGMode == SmoothingGroupImportModeEnum.SmoothAll){
						sg = 0;
					} else if(SGMode == SmoothingGroupImportModeEnum.FlatShading || offSGMode){
						sg = -offSGcounter;
						offSGcounter ++;
					}

					string[] splittedFstring  = str.Split( spaceSeparator, System.StringSplitOptions.RemoveEmptyEntries  );
					RawPolygon rawPolygon = new RawPolygon(sg, SubMeshes.Current);
					for(int i = 1; i<splittedFstring.Length; i++){
						MatchCollection numberMatches = Regex.Matches (splittedFstring[i], @"\d+" );
						if(numberMatches.Count==0) continue;
						if( hasUvData  ){
							rawPolygon.AddCorner(numberMatches[0].Value.ToInt()-1,  numberMatches[1].Value.ToInt()-1  );
						}  else  {
							rawPolygon.AddCorner(numberMatches[0].Value.ToInt()-1, -1);
						} 
	 	 			}
	 	 			tempRawPolygonsList.Add(rawPolygon); 
					
				}
			}
			objFile.Close();
			//sw.Stop();
			///Debug.LogFormat("parse obj time ms: {0}", sw.ElapsedMilliseconds);
	 	}
	
		public void BuildReferenceMesh(){
			//System.Diagnostics.Stopwatch sw = System.Diagnostics.Stopwatch.StartNew();
			for(int i = 0; i<tempRawPolygonsList.Count; i++){
				SubMeshes.AddPolygon( this, tempRawPolygonsList[i] );
			}

			for(int n = 0; n<Normals.Count; n++){
				Normals[n].FillAdjacentNormals();
			}

			//sw.Stop();
			//Debug.LogFormat("add polygons ms: {0}", sw.ElapsedMilliseconds);

			refMesh = new Mesh();
			refMesh.name = "ref mesh";
			Vector3[] umVertices = new Vector3[UnityVertices.Count];
 
			refMesh.vertices = umVertices;

			//UVS
			if(hasUvData){
				Vector2[] umUVs = new  Vector2[UnityVertices.Count];
				for(int v = 0; v<UnityVertices.Count; v++){
					umUVs[v] = UnityVertices[v].UV.Value;
				}
				refMesh.uv = umUVs;
			}
 
			//Debug.LogFormat("has uv: {0}", hasUvData);

			Vector3[] umNormals = new  Vector3[UnityVertices.Count];
 
			refMesh.normals = umNormals;
			refMesh.subMeshCount = SubMeshes.Count;
			for(int m = 0; m<SubMeshes.Count; m++){
				List<int> trisList = new List<int>();
				for(int p = 0; p< SubMeshes[m].Polygons.Count; p++){
					SubMeshes[m].Polygons[p].BuildTriangles( trisList, FlipNormals );
				}
				refMesh.SetTriangles( trisList.ToArray(), m);
			}

 			int[] AllTris = refMesh.triangles;
 			for( int t = 0; t<AllTris.Length; t+=3){
				Face face = new Face();
				face.Va =  UnityVertices[AllTris[t]];
				face.Vb =  UnityVertices[AllTris[t+1]];
				face.Vc =  UnityVertices[AllTris[t+2]];

				for(int p = 0; p<face.Va.Normal.AdjacentPolygons.Count; p++){
					if( face.Va.Normal.AdjacentPolygons[p].IsContainFace ( face )){
						face.ParentPolygon =  face.Va.Normal.AdjacentPolygons[p];
						break;
					}
				}

				for(int p = 0; p<face.Vb.Normal.AdjacentPolygons.Count; p++){
					if( face.Vb.Normal.AdjacentPolygons[p].IsContainFace ( face )){
						face.ParentPolygon =  face.Vb.Normal.AdjacentPolygons[p];
						break;
					}
				}

				for(int p = 0; p<face.Vc.Normal.AdjacentPolygons.Count; p++){
					if( face.Vc.Normal.AdjacentPolygons[p].IsContainFace ( face )){
						face.ParentPolygon =  face.Vc.Normal.AdjacentPolygons[p];
						break;
					}
				}
				Faces.Add(face);
 			}
 			//Debug.LogFormat("added {0} faces, not has parent count: {1}", Faces.Count, notFoundParent );
			//Debug.LogFormat( "UMeshVertices count: {0}, hasUV: {1} polygonsCount:{2} normalsCount:{3}", UnityVertices.Count, hasUvData , SubMeshes.Current.Polygons.Count, Normals.Count );	
		}
	
		public Mesh GetFrame( Vector3[] positions ){
			//Matrix4x4 tm = Matrix4x4.TRS( PivotOffset, Quaternion.identity, Vector3.one*ScaleFactor) ;
			float normalSign = FlipNormals? -1:1;
 
			Mesh result =  (Mesh)Object.Instantiate (refMesh);
			for(int v = 0; v<Vertices.Count; v++){
				 
				Vector3 pos =  positions [v];
				if(SwapYZ){
					pos.Set(pos.x, pos.z, pos.y);
				}
				Vertices [v].Value =   pos * ScaleFactor + PivotOffset;
			}
			Vector3[] umVertices = new Vector3[UnityVertices.Count];
			//VERTICES
			for(int v = 0; v<UnityVertices.Count; v++){
				umVertices[v] = UnityVertices[v].Position.Value;
			}
			refMesh.vertices = umVertices;
			//NORMALS
			for(int p = 0; p<AllPolygons.Count; p++){
				AllPolygons[p].CalcNormal(normalSign);
			}
			Normals.RecalculateAllNormals();

			Vector3[] umNormals = new  Vector3[UnityVertices.Count];
			for(int v = 0; v<UnityVertices.Count; v++){
				umNormals[v] =  UnityVertices[v].Normal.Value;
			}

			result.vertices = umVertices;
			result.normals = umNormals;
			result.RecalculateTangents();
			result.RecalculateBounds ();
			return result;
		}
	}
}
