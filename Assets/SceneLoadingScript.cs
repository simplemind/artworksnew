﻿using System.Collections;
using System.IO;
using System.Collections.Generic;
using UnityEngine;

public class SceneLoadingScript : MonoBehaviour {

    public GameObject lastSplashScreen;
    public string levelName;
    AsyncOperation async;
    private SplashScreenController splashed;
    string externalPersistentDataPath;

    private string _FilesDir;
    private string _CacheDir;
    private string _ExternalFilesDir;
    private string _ExternalCacheDir;
    private string root;


    void Start()
    {

#if !UNITY_EDITOR && UNITY_ANDROID
        using (AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
        {
            using (AndroidJavaObject currentActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity"))
            {
                Debug.Log("using currentActivity");
                /*using (AndroidJavaObject externalDataDirectory = currentActivity.Call<AndroidJavaObject>("getExternalFilesDir", 0)) {
					Debug.Log("using externalFilesDir");
					externalPersistentDataPath = externalDataDirectory.Call<string>("getAbsolutePath"); // could be anything
					Debug.Log("got Path");
					*/
                using (AndroidJavaObject filesDir = currentActivity.Call<AndroidJavaObject>("getFilesDir"))
                {
                    _FilesDir = filesDir.Call<string>("getAbsolutePath");
                    Debug.Log(_FilesDir);

                }

                using (AndroidJavaObject cacheDir = currentActivity.Call<AndroidJavaObject>("getCacheDir"))
                {
                    _CacheDir = cacheDir.Call<string>("getAbsolutePath");
                    Debug.Log(_CacheDir);

                }
                /*
				using( AndroidJavaObject externalFilesDir = currentActivity.Call<AndroidJavaObject>("getExternalFilesDir", null ) )
				{
					_ExternalFilesDir = externalFilesDir.Call<string>("getAbsolutePath");
					Debug.Log (_ExternalFilesDir);
				}

				using( AndroidJavaObject externalCacheDir = currentActivity.Call<AndroidJavaObject>("getExternalCacheDir" ) )
				{
					_ExternalCacheDir = externalCacheDir.Call<string>("getAbsolutePath");
					Debug.Log (_ExternalCacheDir);
				}
				*/
            }
        }
#endif
        StartCoroutine("load");

    }

    IEnumerator load()
    {
        yield return null;
#if !UNITY_EDITOR && UNITY_ANDROID

        externalPersistentDataPath = Application.persistentDataPath;
        Debug.Log("PersistentData Path: " + externalPersistentDataPath);

        //Copy .dat and .xml files where Vuforia can access them
        Debug.Log(Application.streamingAssetsPath);
//#if !UNITY_EDITOR && UNITY_ANDROID
        string uriOfxml = string.Concat(Application.streamingAssetsPath, "/Vuforia/ARtworks.xml"); // do the same same for .dat file too
        string uriOfdat = string.Concat(Application.streamingAssetsPath, "/Vuforia/ARtworks.dat"); // do the same same for .dat file too

        Debug.Log("Downloading xml from " + uriOfxml);
        var wwwXml = new WWW(uriOfxml);
        //Wait until WWW (uriOfxml) is done
        yield return wwwXml;

        Debug.Log("Downloading dat from " + uriOfdat);
        var wwwDat = new WWW(uriOfdat);
        //Wait until WWW (uriOfxml) is done
        yield return wwwDat;
//#endif
        Debug.Log("Writing: " + externalPersistentDataPath + "/ARtworks.xml");
        File.WriteAllBytes(externalPersistentDataPath + "/ARtworks.xml", wwwXml.bytes);
        Debug.Log("Writing: " + externalPersistentDataPath + "/ARtworks.dat");
        File.WriteAllBytes(externalPersistentDataPath + "/ARtworks.dat", wwwDat.bytes);

        //Check if files are copied befire loading scene
        while (!File.Exists(externalPersistentDataPath + "/ARtworks.xml"))
        {
            Debug.Log("XML file does not exist");
            yield return null;
        }
        while (!File.Exists(externalPersistentDataPath + "/ARtworks.dat"))
        {
            Debug.Log("DAT file does not exist");
            yield return null;
        }

        if (File.Exists(externalPersistentDataPath + "/ARtworks.xml") &&
            File.Exists(externalPersistentDataPath + "/ARtworks.dat"))
            Debug.Log("Files exist! Loading main scene");
#endif
        Debug.LogWarning("ASYNC LOAD STARTED - " +
           "DO NOT EXIT PLAY MODE UNTIL SCENE LOADS... UNITY WILL CRASH");
        if (levelName != null)
        {
            async = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(levelName);
        }
        else
            Debug.LogError("No scene to load. Please fill out scene name!");
        async.allowSceneActivation = false;
        yield return async;
    }

    void Update()
    {
        if (lastSplashScreen.activeInHierarchy)
            CheckForSplashed();
        if (splashed != null && async != null)
            if (splashed.isSplashed == true)
            {
                ActivateScene();
            }
    }

    public void ActivateScene()
    {
        async.allowSceneActivation = true;
    }

    private void CheckForSplashed()
    {
        if (lastSplashScreen)
            splashed = lastSplashScreen.GetComponent<SplashScreenController>();
    }
}
