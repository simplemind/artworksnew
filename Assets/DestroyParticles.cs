﻿using UnityEngine;
using System.Collections;
using System.Linq;

[RequireComponent(typeof(ParticleSystem))]
public class DestroyParticles : MonoBehaviour {

    public float DistanceToDestroy;
    private ParticleSystem cachedSystem;
    private Vector3 staticPosition;
    void Start()
    {
        cachedSystem = this.GetComponent<ParticleSystem>();
    }
    void Update()
    {
        staticPosition = this.transform.position;
        ParticleSystem.Particle[] ps = new ParticleSystem.Particle[cachedSystem.particleCount];
        cachedSystem.GetParticles(ps);
        // keep only particles that are within DistanceToDestroy
        var distanceParticles = ps.Where(p => p.position.z > staticPosition.z + 10).ToArray();
        cachedSystem.SetParticles(distanceParticles, distanceParticles.Length);
    }
}
