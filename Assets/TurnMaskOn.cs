﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnMaskOn : MonoBehaviour {

    public GameObject MaskingObject;
	// Use this for initialization
	void Start () {
		if (MaskingObject)
        {
            MaskingObject.SetActive(true);
        }
	}
}
