﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace pc2unity_20{  
	public static class StringExtension  {

		public static int ToInt(this string s){
	 		int result = 0;
	 		int.TryParse( s, out result );
	 		return result;
	 	}

		public static float ToFloat(this string s){
	 		float result = 0;
	 		float.TryParse( s, out result );
	 		return result;
	 	}
	}
}
 
