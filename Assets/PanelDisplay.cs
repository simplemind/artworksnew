﻿using UnityEngine;
using System.Collections;

public class PanelDisplay : MonoBehaviour {

    public GameObject infoPanel;

    public void InfoButton()
    {
        infoPanel.SetActive(!infoPanel.activeInHierarchy);
    }
}
