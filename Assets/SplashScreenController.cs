﻿using UnityEngine;
using System.Collections;

public class SplashScreenController : MonoBehaviour {

    public float splashTime = 2.0f;
    private float startTime;
    public bool isSplashed = false;
    public GameObject nextSplash;

	// Use this for initialization
	void Start () {
        startTime = Time.time;
        StartCoroutine(LogoAnimation());
	}
	
    IEnumerator LogoAnimation() {
        do
        {
            yield return null;
        } while (Time.time <= startTime + splashTime);
        if (nextSplash) {
            nextSplash.SetActive(true);
        }
        isSplashed = true;
        gameObject.SetActive(false);
    }
}
