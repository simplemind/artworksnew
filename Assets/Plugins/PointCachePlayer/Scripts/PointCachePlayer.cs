using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace pc2unity_20{

	public enum PlaybackTypeEnum{
		Once, 
		Loop,
		PingPong,
		Manually 
	}

	public enum SmoothingGroupImportModeEnum{
		FromObjFile,
		FlatShading,
		SmoothAll
	}

	public enum AOQualityEnum {
		Draft,
		Low,
		Medium,
		Hight,
		Ultra
	}

	[ExecuteInEditMode]
	[RequireComponent (typeof(MeshFilter))]
	[RequireComponent (typeof(MeshRenderer))]
	public partial class PointCachePlayer : MonoBehaviour {

		MeshFilter _mf;
		public MeshFilter MF{
			get{
				if(_mf == null){
					_mf = GetComponent<MeshFilter>();
				}
				return _mf;
			}
		}

		MeshCollider _mc;
		public MeshCollider MC{
			get{
				if(_mc == null){
					_mc = GetComponent<MeshCollider>();
				}
				return _mc;
			}
		}

		MeshRenderer _mr;
		public MeshRenderer MR{
			get{
				if(_mr == null){
					_mr = GetComponent<MeshRenderer>();
				}
				return _mr;
			}
		}

		public Mesh CurrentFrameMesh;

		public PlaybackTypeEnum PlaybackMode = PlaybackTypeEnum.PingPong;
		public float FramesPerSecond = 30;
		public int MinFrame;
		public int MaxFrame;
		public float Frame;
		int playbackDirection = 1;
		float timer;
		float frameDuration;
		public Mesh[] Frames = new Mesh[0];
		public bool UpdateMeshColliderIfExist = true;
		 
		public bool ShowImportSettingsUI;
		public bool ShowObjInfo;
		public bool ShowPcInfo;

		public int TabChoise;

		public ResntObjFile ResentObj = new ResntObjFile();
		public ResentPCfile ResentPC = new ResentPCfile();
		public string ResentOutputFile;
		public string Statistic = "<no frames>";

		//import settings
		public bool SwapYZAxis;
		public Vector3 PivotOffset = Vector3.zero;
		public float ScaleFactor = 1f;
		public SmoothingGroupImportModeEnum SmoothingGroupImportMode = SmoothingGroupImportModeEnum.FromObjFile;
		public bool FlipNormals;
		public int importFromFrame = 0;
		public int importToFrame = 7777;
	 	


		//AOSettings
		public bool EnableAO;
		public bool ShowAOGizmos;
		public int SmoothAOIterations = 1;
		public AOQualityEnum AOQuality;
		public float AOOffset = 0.1f;
		public float AORadius = 10f;
		public float AOIntensity = 1f;

		public ObjData currentObjData; 		 

		void LateUpdate(){
			if(Application.isPlaying){
				frameDuration = 1f/FramesPerSecond;
				if(PlaybackMode!=PlaybackTypeEnum.Manually){
					timer += Time.deltaTime;
				}
				if(PlaybackMode == PlaybackTypeEnum.Loop){
					playbackDirection = 1;
				}
				if(timer>frameDuration){
					Frame += (int)(timer/frameDuration)*playbackDirection;
					timer = 0;
				}
				if(Frame >= MaxFrame){
					if(PlaybackMode == PlaybackTypeEnum.Loop){
						Frame = MinFrame;
					} else if(PlaybackMode == PlaybackTypeEnum.Once){
						PlaybackMode = PlaybackTypeEnum.Manually;
					} else if (PlaybackMode == PlaybackTypeEnum.PingPong){
						Frame -=2;
						playbackDirection = -1;
					}
				} else if(Frame <= MinFrame ){
					if (PlaybackMode == PlaybackTypeEnum.PingPong){
						Frame +=2;
						playbackDirection = 1;
					}
				}
			}
			MinFrame = Mathf.FloorToInt( Mathf.Clamp(MinFrame, 0, Frames.Length-1));
			MaxFrame = Mathf.FloorToInt(Mathf.Clamp(MaxFrame, 0, Frames.Length-1));
			Frame =  Mathf.FloorToInt( Mathf.Clamp(Frame, MinFrame, MaxFrame));

 			if(Frames.Length>0){
				MF.mesh =  Frames [(int)Frame];

				if(UpdateMeshColliderIfExist && MC!=null){
					MC.sharedMesh = MF.sharedMesh;
				}
 			}
		}

		public int  FramesCount{
			get{
				return Frames.Length;
			}
		}

 		public void AssignDefaultMaterialsToMeshRender(){
 			if(!FramesIsEmpty){
				Material[] existingMat = MR.sharedMaterials;
				List<Material> nList = new List<Material>();
				int subMeshesCount = FirstFrame.subMeshCount;
				DefaultMaterialsArr dma = new DefaultMaterialsArr();
			 	
				for(int s = 0; s<subMeshesCount; s++){
					if(s>=existingMat.Length ||  existingMat[s] == null){
						nList.Add(dma[s]);
					} else{
						nList.Add(existingMat[s]);
					}
 				}
				MR.sharedMaterials = nList.ToArray();
 			} else {
 				Debug.Log("MF shared mesh == null");
 			}
 		}

 		public bool FramesIsEmpty{
 			get{
 				return Frames == null || Frames.Length == 0 || FirstFrame == null;
 			}
 		}

 		public Mesh FirstFrame{
 			get{
 				return Frames[0];
 			}
 		}

 		public void UpdateStatistic(){
 			if(!FramesIsEmpty){
 				Statistic =  string.Format("{0} frames, ", Frames.Length);
				Statistic +=  string.Format("{0} verts, ", FirstFrame.vertexCount);
				Vector2[] uvarr = FirstFrame.uv ;
				Statistic +=  (uvarr == null) || (uvarr.Length == 0) ? "": "UV, " ;
			 

				string subMeshesInfo = "";
				if(FirstFrame.subMeshCount>1){
					subMeshesInfo = "(";
	 				for(int i = 0; i<FirstFrame.subMeshCount; i++){
						subMeshesInfo +=   (FirstFrame.GetTriangles(i).Length/3).ToString();
						if(i < FirstFrame.subMeshCount-1){
							subMeshesInfo +="+";
						} else {
							subMeshesInfo += ")";
						}
	 				}
 				}
				Statistic +=  string.Format(" {0} tris {1}", FirstFrame.triangles.Length/3,  subMeshesInfo); 
 			} else {
 				Statistic = "no frames";
 			}
 		}
	
		public bool ImportRuntime(string objPath, string pcPath ){
			
			bool objExist = System.IO.File.Exists(objPath);
			bool pcExist = System.IO.File.Exists(pcPath);
			if(!objExist  ){
				Debug.LogFormat("{0} not found", objPath);
				return false;
			}

			if(!pcExist  ){
				Debug.LogFormat("{0} not found", pcPath);
				return false;
			}

			ObjData obj = new ObjData (objPath, PivotOffset, SmoothingGroupImportMode, ScaleFactor, FlipNormals, SwapYZAxis);
			obj.BuildReferenceMesh();
			PointCacheData pc = new PointCacheData (pcPath);

			if(pc.VerticesCount != obj.Vertices.Count){
				Debug.Log("Vertex count mismatch");
				return false;
			}

			Frames = new Mesh[pc.FramesCount];
			for(int i = 0; i<Frames.Length; i++){
				Frames[i] = obj.GetFrame(pc.GetFrame(i));
				if(EnableAO){
					ApplyAO(Frames[i], obj);
				}
			}
			return true;
		}

		void OnDrawGizmosSelected(){
			if(ShowAOGizmos && !FramesIsEmpty){
				Color c = Color.red;
				c.a = 0.5f;
				Gizmos.color = c;
				Vector3[] verts = Frames [(int)Frame].vertices;
				Vector3[] norms = Frames [(int)Frame].normals;
				Matrix4x4 objTM = transform.localToWorldMatrix;
				for( int i = 0; i<verts.Length; i++ ){
					Vector3 a =  objTM.MultiplyPoint3x4( verts[i] + norms[i] * AOOffset );
					Vector3 b =  objTM.MultiplyPoint3x4( verts[i] + norms[i] * AORadius );
					Gizmos.DrawLine( a, b );
				}

			}

		}


		public Bounds AnimationBounds{
			get{
				Bounds result = new Bounds(Vector3.zero, Vector3.zero);
				if(!FramesIsEmpty){
					for(int i = 0; i<Frames.Length; i++){
						result.Encapsulate( Frames[i].bounds.min);
						result.Encapsulate( Frames[i].bounds.max);
					}
				}
				return result;
			}
		}
	}
}
