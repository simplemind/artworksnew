﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

namespace pc2unity_20{

	[System.Serializable] 
	public class ResentPCfile : ResentFile {
		public int VerticesCount;
		public int FramesCount;

		public override bool FillStatistic  (string path){
			FileStream fs = new FileStream(path, FileMode.Open);
			BinaryReader binReader = new BinaryReader(fs);
		 
			binReader.ReadChars(12);
			binReader.ReadInt32();
			VerticesCount = binReader.ReadInt32();
			binReader.ReadSingle();//start frame
			binReader.ReadSingle();//sample rate
			FramesCount = binReader.ReadInt32();
 
			fs.Close();
			binReader.Close();

 
			StatisticString =  "Vertices count: " + VerticesCount.ToString();
			StatisticString += "\nFrames count:  " + FramesCount.ToString();
			StatisticString += "\nmodified time :  "  +  (new FileInfo(path)).LastWriteTime.ToString();
			return true;	
	 	}
	}
}
