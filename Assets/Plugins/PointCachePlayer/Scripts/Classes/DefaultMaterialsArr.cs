﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace pc2unity_20{
	public class DefaultMaterialsArr  {
		Material[] arr;

		public DefaultMaterialsArr(){
			Object[] items = Resources.LoadAll( "pc2unityDefaultResources");
			List<Material> matList = new List<Material>();
			for(int i = 0; i<items.Length; i++){
				if(items[i].name.StartsWith("Pc2unityDefaultMat_")){
					Material mat = (Material)items[i];
					matList.Add(mat);
					//Debug.LogFormat("added mat: {0}", mat);
				}
			}
			arr = matList.ToArray();
		}


		public int Count{
			get{
				return arr.Length;
			}
		}

		public Material this[int idx]{
			get{
				return arr[idx%arr.Length];
			}
		}

	}




}
