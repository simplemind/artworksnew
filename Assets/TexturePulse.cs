﻿using UnityEngine;
using System.Collections;

public class TexturePulse : MonoBehaviour {

    Material localMaterial;

    public float minBound = 0.0f;
    public float maxBound = 0.5f;
    float currentVal;
    public float duration = 5.0f;
    private float startTime;

	// Use this for initialization
	void Start () {
        localMaterial = GetComponent<MeshRenderer>().material;
        localMaterial.EnableKeyword("_EMISSION");
        currentVal = localMaterial.GetColor("_EmissionColor").grayscale;
        startTime = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
        if (Time.time - startTime > duration)
        {
            startTime = Time.time;
            float f = minBound;
            minBound = maxBound;
            maxBound = f;
            duration = Random.Range(5.0f, 8.0f);
        }
            
        float t = Mathf.SmoothStep(minBound, maxBound, (Time.time - startTime) / duration);
        localMaterial.SetColor("_EmissionColor", new Color(t,t,t,t));
	}
}
