﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;


namespace pc2unity_20 {
 
	[System.Serializable]
	public class ResntObjFile : ResentFile {

		public string ObjectName;
		public int VerticesCount;
		public int SmoothingGroupCount;
		public int OffSmothingGroups;
		public string[] Materials;
	 
		public override bool FillStatistic  (string path){
			ObjectName = "";
			VerticesCount = 0;
			SmoothingGroupCount = 0;
			OffSmothingGroups = 0;
			Materials = new string[0];

			System.IO.TextReader objFile = System.IO.File.OpenText(path);
			 
			List<string> sg = new List<string>();
			List<string> mats = new List<string>();
			bool currentHasSmoothingGroup = false;
			while (true){
				string str = objFile.ReadLine();
				if(str == null) break;
				if(str.StartsWith("# object")){
					ObjectName = str.Remove(0,8);
				} else if (str.StartsWith("v ")){
					 VerticesCount ++;				
				} else if( str.StartsWith ("s off")){
					currentHasSmoothingGroup = false;
				} else if ( str.StartsWith ("s " ) ){
					currentHasSmoothingGroup = true;
					bool smoothingGroupExist = false;
					for(int i = 0; i<sg.Count; i++ ){
						if(sg[i] == str){
							smoothingGroupExist = true;
							break;
						}
					}
					if(!smoothingGroupExist){
						sg.Add( str );
					}
				} else if( currentHasSmoothingGroup == false && str.StartsWith("f ")){
					 OffSmothingGroups ++;
				}
				else if (str.StartsWith("usemtl")){
					string matName = str.Remove(0,7);
					bool exist = false;
					for(int i = 0; i<mats.Count; i++){
						if( mats[i] == matName){
							exist = true;
							break;
						} 
					}
					if(!exist){
						mats.Add( matName );
					}
				}
				Materials = mats.ToArray();
				SmoothingGroupCount = sg.Count;
			}
			objFile.Close();

			StatisticString =  "object:  "+ObjectName;
			StatisticString +=  "\nvertices:  " + VerticesCount.ToString();
			StatisticString += "\nsmoothing groups:  " + SmoothingGroupCount.ToString();
			StatisticString += "\noff smoothing groups:  " + OffSmothingGroups.ToString();
			StatisticString += "\nmaterials (submeshes):  "  + Materials.Length.ToString();
			StatisticString += "\nmodified time :  "  +  (new FileInfo(path)).LastWriteTime.ToString();

			//Debug.Log("Filled new obj statistic");
		 	return true;
		}


	}
}
