﻿using UnityEngine;
using System.Collections;

public class UrlButtons : MonoBehaviour {

	public void OpenRufi()
    {
        Application.OpenURL("http://unicat.co/products/ioana-nicoara-rufi");
    }

    public void OpenLaundry()
    {
        Application.OpenURL("http://unicat.co/products/dan-badea-laundry-ii");
    }

    public void OpenPlayground()
    {
        Application.OpenURL("http://unicat.co/products/dominic-virtosu-playground");
    }

    public void OpenAbstract()
    {
        Application.OpenURL("http://unicat.co/products/kitra-abstract");
    }
}
