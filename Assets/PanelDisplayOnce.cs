﻿using UnityEngine;
using System.Collections;

public class PanelDisplayOnce : MonoBehaviour {

    public GameObject infoPanel;
	// Use this for initialization
	void Start () {
	    if (PlayerPrefs.GetInt("IntroDisplayed", 0) == 0)
        {
            PlayerPrefs.SetInt("IntroDisplayed", 1);
            infoPanel.SetActive(true);
        } else
        {
            infoPanel.SetActive(false);
        }
	}
	
}
