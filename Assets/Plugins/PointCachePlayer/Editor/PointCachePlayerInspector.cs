﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.AnimatedValues;
using System.IO;
 

namespace pc2unity_20{

	[CustomEditor(typeof(PointCachePlayer))]
	public sealed partial class Pc2unitycontrollerEditor : Editor {
 
 
		static AnimBool showObjInfoAB;
		static AnimBool showPcInfoAB;
		static AnimBool showAoSettingsAB;


		string lastOpenedAssetDirectory = "";
		string lastOpenedObjDirectory = "";
		string lastOpenedPcDirectory = "";

 		string[] tabsNames;
  						

		void OnEnable(){
			PointCachePlayer t = (PointCachePlayer)target;
			showObjInfoAB = new AnimBool(t.ShowObjInfo, Repaint);
			showPcInfoAB = new AnimBool(t.ShowPcInfo, Repaint);
			showAoSettingsAB = new AnimBool(t.EnableAO, Repaint);


			lastOpenedAssetDirectory = EditorPrefs.GetString( "pc2unitylastOpenedAssetDirectory" );
			lastOpenedObjDirectory = EditorPrefs.GetString( "pc2unitylastOpenedObjDirectory" );
			lastOpenedPcDirectory = EditorPrefs.GetString( "pc2unitylastOpenedPcDirectory" );
			//sw = System.Diagnostics.Stopwatch.StartNew(); 
			tabsNames = new string[3]{"Playback", "Import", "Load existing"};
 			
 			t.UpdateStatistic();
		}

		public override void OnInspectorGUI () {
			serializedObject.Update();
			PointCachePlayer t = (PointCachePlayer)target;
 			t.ResentObj.FilePath = t.ResentObj.FilePath;
			t.ResentPC.FilePath = t.ResentPC.FilePath;

			bool changed = false;
			EditorGUILayout.HelpBox( t.Statistic, MessageType.None );

			int ntabChoise = GUILayout.Toolbar( t.TabChoise, tabsNames);
			if(ntabChoise != t.TabChoise){
				t.TabChoise = ntabChoise;
				changed = true;
			}
			if( t.TabChoise == 0) {
				if(t.FramesIsEmpty){
					EditorGUILayout.HelpBox( "Frames not loaded yet. Please load existing frames asset or import new", MessageType.Warning );
				} else {
					PlaybackInspector( ref changed , t);
				}
			} else if ( t.TabChoise == 1){
				ImportInspector( ref changed, t);
			} else if ( t.TabChoise == 2 ){
				LoadExistingInspector(ref changed, t );
			}


 				

			if(changed && !Application.isPlaying){
				EditorUtility.SetDirty(t);
				UnityEditor.SceneManagement.EditorSceneManager.MarkAllScenesDirty();
			}

			
				
 			serializedObject.ApplyModifiedProperties();
		}

		void ImportInspector ( ref bool changed, PointCachePlayer t){
			//OBJ 

			GUILayout.Label(".obj file:", "BoldLabel");
			GUILayout.BeginVertical("HelpBox");

			//SHOW OBJ INFO TOGGLE
			GUILayout.BeginHorizontal(); 
			bool nShowObjInfo = GUILayout.Toggle( t.ShowObjInfo, "info", "Button", GUILayout.Width(35));
			if(nShowObjInfo != t.ShowObjInfo){
				showObjInfoAB.target = nShowObjInfo;
				t.ShowObjInfo = nShowObjInfo;
				changed = true;
			}
 
 			//SELECT OBJ BUTTON
			if(GUILayout.Button(t.ResentObj.ButtonName)){
				string dir = t.ResentObj.IsValidDirectory? t.ResentObj.DirectoryPath : lastOpenedObjDirectory;
				t.ResentObj.FilePath  = EditorUtility.OpenFilePanelWithFilters ("Select obj file", dir, new string[2]{ "OBJ","obj" });
					if(t.ResentObj.state == ResentFileState.ReadyForImport){
					EditorPrefs.SetString( "pc2unitylastOpenedObjDirectory", t.ResentObj.DirectoryPath );
 				}
 				changed = true;
 			}
			GUILayout.EndHorizontal();

			//OBJ INFO LABEL 
			if(t.ResentObj.state == ResentFileState.ReadyForImport){
				if( EditorGUILayout.BeginFadeGroup(showObjInfoAB.faded)){
					GUILayout.Label(t.ResentObj.StatisticString);
				}
				EditorGUILayout.EndFadeGroup();
			}
			GUILayout.EndVertical();


			//PC
	
			GUILayout.Label(".pc file:", "BoldLabel");
			GUILayout.BeginVertical("HelpBox");

			//SHOW OBJ INFO TOGGLE
			GUILayout.BeginHorizontal(); 
			bool nShowPcObjInfo = GUILayout.Toggle( t.ShowPcInfo, "info", "Button", GUILayout.Width(35));
			if(nShowPcObjInfo != t.ShowPcInfo){
				showPcInfoAB.target = nShowPcObjInfo;
				t.ShowPcInfo = nShowPcObjInfo;
				changed = true;
			} 


			if(GUILayout.Button(t.ResentPC.ButtonName)){
				string dir = t.ResentPC.IsValidDirectory? t.ResentPC.DirectoryPath : lastOpenedPcDirectory;
				t.ResentPC.FilePath  = EditorUtility.OpenFilePanelWithFilters ("Select point cache file", dir, new string[2]{ "PC2","pc2" });
				if( t.ResentPC.state == ResentFileState.ReadyForImport ){
					EditorPrefs.SetString( "pc2unitylastOpenedPcDirectory", t.ResentPC.DirectoryPath );
					Debug.LogFormat("Choised new pc");
				}
				changed = true;
 			}
 			GUILayout.EndHorizontal();


			if(t.ResentPC.state == ResentFileState.ReadyForImport){
				if( EditorGUILayout.BeginFadeGroup(showPcInfoAB.faded)){
					GUILayout.Label(t.ResentPC.StatisticString);
				}
				EditorGUILayout.EndFadeGroup();
			}
			GUILayout.EndVertical();

			//OUTPUT

			GUILayout.Label(" frames asset output:", "BoldLabel");
			 
			string buttonName = "none";
			if(t.ResentOutputFile!= null && t.ResentOutputFile.Length>0){
				buttonName = t.ResentOutputFile;
			}

			if(GUILayout.Button( buttonName )){
				string selectedFile = EditorUtility.SaveFilePanelInProject("Save asset", "", "asset", "Please select a file to save frames to");
				if( selectedFile != null || selectedFile.Length>0 ){
					t.ResentOutputFile = selectedFile;
				}
				changed = true;
 			}
			 
			bool objReady = t.ResentObj.state == ResentFileState.ReadyForImport;
			bool pcReady = t.ResentPC.state == ResentFileState.ReadyForImport;
			if(objReady && pcReady){
				bool nFlipNormals = EditorGUILayout.Toggle("FlipNormals", t.FlipNormals );
				if(nFlipNormals != t.FlipNormals){
					t.FlipNormals = nFlipNormals;
					changed = true;
				}

				bool nSwapYZ = EditorGUILayout.Toggle("Swap YZ axiz", t.SwapYZAxis );
				if(nSwapYZ != t.SwapYZAxis){
					t.SwapYZAxis = nSwapYZ;
					changed = true;
				}

				float nScaleFactor = EditorGUILayout.FloatField("Scale factor", t.ScaleFactor );
				if(nScaleFactor != t.ScaleFactor){
					t.ScaleFactor = nScaleFactor;
					changed = true;
				}

				SmoothingGroupImportModeEnum nsgMode = (SmoothingGroupImportModeEnum)EditorGUILayout.EnumPopup("Smoothing group mode", t.SmoothingGroupImportMode );
				if(nsgMode != t.SmoothingGroupImportMode) {
					t.SmoothingGroupImportMode = nsgMode;
					changed = true;
				}

				Vector3 nPivotOffset = EditorGUILayout.Vector3Field("Pivot offset", t.PivotOffset);
				if(nPivotOffset != t.PivotOffset){
					t.PivotOffset = nPivotOffset;
					changed = true;
				}

				float nFromFrame = t.importFromFrame;
				float nToFrame =  t.importToFrame;

				if(t.ResentPC.state == ResentFileState.ReadyForImport){
					EditorGUILayout.MinMaxSlider(string.Format("Import range [{0}-{1}]", t.importFromFrame, t.importToFrame), ref nFromFrame, ref nToFrame, 0, t.ResentPC.FramesCount ); 
					nToFrame = Mathf.Clamp(nToFrame, 0, t.ResentPC.FramesCount);
				}

				if(nFromFrame != t.importFromFrame){
					t.importFromFrame = (int)nFromFrame;
					changed = true;
				} 

				if(nToFrame != t.importToFrame){
					t.importToFrame = (int)nToFrame;
					changed = true;
				} 

				AOSettingsInspector(ref changed, t);
					
				if(GUILayout.Button("Import!")){
					ImportFrames();
					changed = true;
				}
			}
		}

		void AOSettingsInspector(ref bool changed, PointCachePlayer t){
			bool nEnableAO = GUILayout.Toggle(  t.EnableAO, "Enable Ambient Occlusion", "Button");
			if( nEnableAO !=  t.EnableAO){
				showAoSettingsAB.target = nEnableAO;
				t.EnableAO = nEnableAO;
				changed = true;
			}
	 
			if( EditorGUILayout.BeginFadeGroup( showAoSettingsAB.faded)){
				AOQualityEnum nAOquality = (AOQualityEnum)EditorGUILayout.EnumPopup("Quality:", t.AOQuality); 
				if(nAOquality != t.AOQuality){
					t.AOQuality = nAOquality;
					changed = true;
				}


				float nAOOffset = EditorGUILayout.Slider("Ray offset", t.AOOffset, 0, 1f);
				if(nAOOffset != t.AOOffset){
					t.AOOffset = nAOOffset;
					changed = true;
				}

				float nAOORadius = EditorGUILayout.Slider("Radius", t.AORadius, 0, 100f);
				if(nAOORadius != t.AORadius){
					t.AORadius = nAOORadius;
					changed = true;
				}

				int nSmoothIterations = EditorGUILayout.IntSlider( "Smooth iterations", t.SmoothAOIterations, 0, 7 );
				if(nSmoothIterations != t.SmoothAOIterations){
					t.SmoothAOIterations = nSmoothIterations;
					changed = true;
				}



				float nAOintesity = EditorGUILayout.Slider("Intensity", t.AOIntensity, 0, 10f);
				if(nAOintesity != t.AOIntensity){
					t.AOIntensity = nAOintesity;
					changed = true;
				}

				bool nShowGizmos = EditorGUILayout.Toggle( "show gizmos", t.ShowAOGizmos);
				if(nShowGizmos != t.ShowAOGizmos){
					t.ShowAOGizmos = nShowGizmos;
					changed = true;
				}
			}
			EditorGUILayout.EndFadeGroup();
		}

		void PlaybackInspector (ref bool changed, PointCachePlayer t){
			PlaybackTypeEnum nPlaybackType = (PlaybackTypeEnum)EditorGUILayout.EnumPopup("Playback type:", t.PlaybackMode );
			if(nPlaybackType != t.PlaybackMode){
				t.PlaybackMode = nPlaybackType;
				changed = true;
			}
			  
			float nFrame = EditorGUILayout.Slider("Frame:",   t.Frame ,  t.MinFrame  ,   t.MaxFrame  );
			if(nFrame != t.Frame){
				t.Frame = nFrame;
				changed = true;
			}

			float nMinFrame = t.MinFrame;
			float nMaxFrame = t.MaxFrame;
			EditorGUILayout.MinMaxSlider("Range", ref nMinFrame, ref nMaxFrame, 0, t.FramesCount-1 );
			if(nMinFrame != t.MinFrame || nMaxFrame != t.MaxFrame){
				t.MinFrame = (int)nMinFrame;
				t.MaxFrame = (int)nMaxFrame;
				changed = true;
			}
			int nFramesPerSecond =  (int)EditorGUILayout.Slider("Framerate:", t.FramesPerSecond, 0, 120 );
			if(t.FramesPerSecond != nFramesPerSecond){
				t.FramesPerSecond = nFramesPerSecond;
				changed = true;
			}

			bool nUpdateMeshCollider = EditorGUILayout.Toggle("Update Mesh Collider", t.UpdateMeshColliderIfExist );
			if(nUpdateMeshCollider !=  t.UpdateMeshColliderIfExist){
				t.UpdateMeshColliderIfExist = nUpdateMeshCollider;
				changed = true;
			}
		}

		void LoadExistingInspector(ref bool changed, PointCachePlayer t){
			if(GUILayout.Button("Select existing")){
				string assetPath  = EditorUtility.OpenFilePanelWithFilters ("Select frames", lastOpenedAssetDirectory, new string[2]{ "ASSET","asset" });
		 		if ( LoadExistingFrames( assetPath, t, false)){
		 			changed = true;
					FileInfo fi = new FileInfo( assetPath );
					lastOpenedAssetDirectory = fi.Directory.Name;
		 		}
		 	}
		}

		bool LoadExistingFrames(string assetPath, PointCachePlayer t, bool isLocalPath){
			if(assetPath.Length==0){
				return false;
			}

			if(!isLocalPath){
				assetPath =  "Assets" + assetPath.Substring(Application.dataPath.Length);
				EditorPrefs.SetString( "pc2unitylastOpenedAssetDirectory", assetPath );
			}
			 
 				
			Object[] assets = AssetDatabase.LoadAllAssetsAtPath(assetPath);
			if(assets == null){
				Debug.LogError("frames asset file not found");
				return false;
			}
			if(!assets.GetType().IsArray ){
				Debug.LogError("frames asset not valid");
				return false;
			}

			Mesh[] loadedFrames = new Mesh[assets.Length-1];
			int findedMeshesCount = 0;					
 			for(int i = 0; i<assets.Length; i++){
				int index = -1;
				if( assets[i].GetType() != typeof(Mesh) ){
					Debug.LogError("frames asset contain non mesh element");
					return false;
				}

				Mesh m = (Mesh)assets[i];
				if(m.name.StartsWith("frame #")){
					int.TryParse(m.name.Remove(0,7), out index);
					loadedFrames[index] =  m;
					findedMeshesCount ++;
				}  
			}

			if(findedMeshesCount != loadedFrames.Length){
				Debug.LogError("frames asset has wrong names");
				return false;
			}

			t.Frames = loadedFrames;
			t.MinFrame = 0; 
			t.MaxFrame = t.FramesCount;	 
			t.FramesPerSecond = 30;
 			t.UpdateStatistic();
			return true;
		}



		void OnDisable(){
			EditorPrefs.SetString("pc2unitylastOpenedExistingAssetDirectory", lastOpenedAssetDirectory );
		}


		bool ImportFrames(  ){
			PointCachePlayer t = (PointCachePlayer)target;
			//check files
			t.ResentObj.FilePath = t.ResentObj.FilePath;
			if(t.ResentObj.state != ResentFileState.ReadyForImport){
				EditorUtility.DisplayDialog ("Obj file error", t.ResentObj.state.ToString(), "OK" );
				EditorUtility.ClearProgressBar ();
				return false;
			} 

			t.ResentPC.FilePath = t.ResentPC.FilePath;
			if(t.ResentPC.state != ResentFileState.ReadyForImport){
				EditorUtility.DisplayDialog ("Point cache file error", t.ResentPC.state.ToString(), "OK" );
				EditorUtility.ClearProgressBar ();
				return false;
			} 

			if(t.ResentObj.VerticesCount != t.ResentPC.VerticesCount){
				string str =  string.Format("obj verts count: {0}, point cache verts count: {1}", t.ResentObj.VerticesCount, t.ResentPC.VerticesCount) ;
				EditorUtility.DisplayDialog ("Vertex count mismatch", str, "OK" );
				EditorUtility.ClearProgressBar ();
				return false;
			}

			EditorUtility.DisplayProgressBar("Importing", "Parsing obj file", 0);
			t.currentObjData = new ObjData (t.ResentObj.FilePath, t.PivotOffset, t.SmoothingGroupImportMode, t.ScaleFactor, t.FlipNormals, t.SwapYZAxis);

			EditorUtility.DisplayProgressBar("Importing", "Building reference mesh", 0.1f);
			t.currentObjData.BuildReferenceMesh ();
			EditorUtility.DisplayProgressBar("Importing", "Reading point cache", 0.2f);
			PointCacheData pc = new PointCacheData (t.ResentPC.FilePath);
			EditorUtility.DisplayProgressBar("Importing", "Building frames frame", 0.3f);

			try {
				AssetDatabase.CreateAsset( new Mesh(), t.ResentOutputFile);
			} catch{
				EditorUtility.DisplayDialog ("Writing asset error", t.ResentOutputFile, "OK" );
				EditorUtility.ClearProgressBar ();
				return false;
			}
			t.MF.mesh = null;
			int counter = 0;
			int rangeLength = t.importToFrame - t.importFromFrame;

			for(int f = t.importFromFrame; f< t.importToFrame; f++){
				Mesh frameMesh = t.currentObjData.GetFrame (pc.GetFrame(f) );
				if(t.EnableAO){
					t.ApplyAO(frameMesh, t.currentObjData);
				}
				t.MF.sharedMesh = frameMesh;
				frameMesh.name = "frame #" + counter.ToString();

				AssetDatabase.AddObjectToAsset( frameMesh, t.ResentOutputFile );
	
				float _progress = 0.3f + (float)counter/rangeLength*0.7f;
				EditorUtility.DisplayProgressBar("Importing", string.Format( "Building frame {0} of {1}", counter.ToString(), rangeLength),  _progress);
				counter ++;
			}
			EditorUtility.DisplayProgressBar("Importing", "Save frames", 1f);
			AssetDatabase.SaveAssets();

			AssetDatabase.Refresh();

			EditorUtility.DisplayProgressBar("Importing", "Linking frames with player", 1f);
			LoadExistingFrames (t.ResentOutputFile, t, true);

			EditorUtility.ClearProgressBar ();

			t.AssignDefaultMaterialsToMeshRender();
			t.UpdateStatistic();
 
			return true;
		}


		[MenuItem("GameObject/3D Object/PointCachePlayer")]
	 	public static void CreateNew(){
	 		GameObject go = new GameObject("New PointCache player");
	 		go.AddComponent<PointCachePlayer>();
			Material defaultMat = (Material)Resources.Load ("pc2unityDefaultResources/Pc2unityDefaultMat_0");
			if (defaultMat == null) {
				defaultMat = (Material)AssetDatabase.GetBuiltinExtraResource<Material>("Default-Diffuse.mat");
			}
			go.GetComponent<MeshRenderer>().material = defaultMat;
			Selection.objects = new Object[1]{go};
		}

	}	
}
