﻿using UnityEngine;
using System.Collections;
using System.IO;
using UnityEngine.UI;

public class TakeScreenshots : MonoBehaviour 
{
	[HideInInspector] public bool takeScreenshot = false;
	public GameObject cameraButton;
	public GameObject screenImage;
    public string fileName = "Screen-"; //start of file name; later the current time is added for unique identifier
    public string pathToGo = "/GSK_AR/"; //Could change this name to any other; is the folder in the root directory of the (emulated) sdcard where screens are saved

    string crossoverFileName, destination, root, origin;
    bool creatingFile = false;

    void Start() {
#if UNITY_ANDROID && !UNITY_EDITOR
 		AndroidJavaClass Environment = new AndroidJavaClass("android.os.Environment");
		using (AndroidJavaObject externalStorageDirectory = Environment.CallStatic<AndroidJavaObject>("getExternalStorageDirectory"))
			root = externalStorageDirectory.Call<string>("getAbsolutePath"); // could be anything
		string dcimDirectory = Environment.GetStatic<string>("DIRECTORY_DCIM");

        crossoverFileName = root + "/" + dcimDirectory + pathToGo;
        Directory.CreateDirectory(crossoverFileName);
        if (Directory.Exists(crossoverFileName))
            Debug.Log(crossoverFileName);
        else
            Debug.Log("NoPathToGo");
#endif
    }


    public void TakeScreenshot()
	{
		takeScreenshot = true;

	}


	void LateUpdate()
	{
		if (takeScreenshot) 
		{
            string screenFileName = fileName + System.DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss") + ".png";
#if UNITY_ANDROID && !UNITY_EDITOR
            creatingFile = true;
            ScreenCapture.CaptureScreenshot(screenFileName);
            if (creatingFile)
            {
                origin = Path.Combine(Application.persistentDataPath, screenFileName);
                Debug.Log("origin: " + origin);
                destination = crossoverFileName + screenFileName;
                //				callTextWindow (origin + "__result: " + destination);
                Debug.Log("destination: " + destination);

            }
#endif

            AnimateTexture();
			Debug.Log ("ScreenShot Taken!");
			takeScreenshot = false;

#if UNITY_ANDROID && !UNITY_EDITOR
			if (cameraButton != null) cameraButton.GetComponent<Button> ().interactable = false;
#endif

        }
    }

	// Update is called once per frame
	void Update ()
    {
        if (creatingFile)
        {
            //Debug.Log("waiting");
            if (File.Exists(origin))
            {
#if UNITY_ANDROID && !UNITY_EDITOR
                //callTextWindow ("SCREENSHOT TAKEN!");
                Debug.Log("FileExists! " + origin);
                System.IO.File.Move(origin, destination);
                if (System.IO.File.Exists(destination))
                {
                    Debug.Log("DestinationFileExists! " + destination);
                }
                creatingFile = false;


                using (AndroidJavaClass jcUnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
                using (AndroidJavaObject joActivity = jcUnityPlayer.GetStatic<AndroidJavaObject>("currentActivity"))
                using (AndroidJavaObject joContext = joActivity.Call<AndroidJavaObject>("getApplicationContext"))
                using (AndroidJavaClass jcMediaScannerConnection = new AndroidJavaClass("android.media.MediaScannerConnection"))
                using (AndroidJavaClass jcEnvironment = new AndroidJavaClass("android.os.Environment"))
                {
                    jcMediaScannerConnection.CallStatic("scanFile", joContext, new string[] { destination }, null, null);
                }
#endif

                if (cameraButton != null) cameraButton.GetComponent<Button>().interactable = true;
            }
        }
    }

    void AnimateTexture ()
    {
        if (screenImage != null)
        {
            Debug.Log("Making texture");
            Texture2D texture = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
            texture.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0, false);
            texture.Apply();
            Sprite screenSprite = Sprite.Create(texture, new Rect(0, 0, Screen.width, Screen.height), new Vector2(0, 0));
            screenImage.GetComponent<Image>().enabled = true;
            screenImage.GetComponent<Image>().sprite = screenSprite;
            screenImage.GetComponent<Animator>().Play("DownAndOut");
            StartCoroutine(killImage(screenImage.GetComponent<Image>()));
        }
	}

    IEnumerator killImage(Image imageToKill) {
		yield return new WaitForSeconds(1.6f);

		imageToKill.enabled = false;
	}



}

