﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace pc2unity_20{
	public partial class PointCachePlayer : MonoBehaviour {

		#region AODirs_126
		public static readonly Vector3[] aoDirs_126 = new Vector3[126] {
		      new Vector3(0.0f, 0.0f, 1.0f ) , 
		      new Vector3(0.894427f, 0.0f, 0.447214f ) , 
		      new Vector3(0.276393f, 0.850651f, 0.447214f ) , 
		      new Vector3(-0.723607f, 0.525731f, 0.447214f ) , 
		      new Vector3(-0.723607f, -0.525731f, 0.447214f ) , 
		      new Vector3(0.276393f, -0.850651f, 0.447214f ) , 
		      new Vector3(0.219625f, 0.0f, 0.975584f ) , 
		      new Vector3(0.428525f, 0.0f, 0.90353f ) , 
		      new Vector3(0.6165f, 0.0f, 0.787355f ) , 
		      new Vector3(0.77437f, 0.0f, 0.632733f ) , 
		      new Vector3(0.0678678f, 0.208875f, 0.975584f ) , 
		      new Vector3(0.132421f, 0.407551f, 0.90353f ) , 
		      new Vector3(0.190509f, 0.586326f, 0.787355f ) , 
		      new Vector3(0.239294f, 0.73647f, 0.632733f ) , 
		      new Vector3(-0.17768f, 0.129092f, 0.975584f ) , 
		      new Vector3(-0.346684f, 0.251881f, 0.90353f ) , 
		      new Vector3(-0.498759f, 0.362369f, 0.787355f ) , 
		      new Vector3(-0.626479f, 0.455163f, 0.632733f ) , 
		      new Vector3(-0.17768f, -0.129092f, 0.975584f ) , 
		      new Vector3(-0.346684f, -0.251881f, 0.90353f ) , 
		      new Vector3(-0.498759f, -0.362369f, 0.787355f ) , 
		      new Vector3(-0.626479f, -0.455163f, 0.632733f ) , 
		      new Vector3(0.0678678f, -0.208875f, 0.975584f ) , 
		      new Vector3(0.132422f, -0.407551f, 0.90353f ) , 
		      new Vector3(0.190509f, -0.586326f, 0.787355f ) , 
		      new Vector3(0.239294f, -0.73647f, 0.632733f ) , 
		      new Vector3(0.842238f, 0.208875f, 0.496997f ) , 
		      new Vector3(0.748921f, 0.407551f, 0.522512f ) , 
		      new Vector3(0.619034f, 0.586326f, 0.522512f ) , 
		      new Vector3(0.458918f, 0.73647f, 0.496997f ) , 
		      new Vector3(0.0616134f, 0.865562f, 0.496997f ) , 
		      new Vector3(-0.156175f, 0.838207f, 0.522512f ) , 
		      new Vector3(-0.366337f, 0.769921f, 0.522512f ) , 
		      new Vector3(-0.558611f, 0.664039f, 0.496997f ) , 
		      new Vector3(-0.804159f, 0.326071f, 0.496997f ) , 
		      new Vector3(-0.845443f, 0.110489f, 0.522512f ) , 
		      new Vector3(-0.845443f, -0.110489f, 0.522512f ) , 
		      new Vector3(-0.804159f, -0.326071f, 0.496997f ) , 
		      new Vector3(-0.558611f, -0.664039f, 0.496997f ) , 
		      new Vector3(-0.366337f, -0.769921f, 0.522512f ) , 
		      new Vector3(-0.156175f, -0.838207f, 0.522512f ) , 
		      new Vector3(0.0616135f, -0.865562f, 0.496997f ) , 
		      new Vector3(0.458918f, -0.73647f, 0.496997f ) , 
		      new Vector3(0.619034f, -0.586326f, 0.522512f ) , 
		      new Vector3(0.748921f, -0.407551f, 0.522512f ) , 
		      new Vector3(0.842238f, -0.208875f, 0.496997f ) , 
		      new Vector3(0.95205f, 0.129092f, 0.277373f ) , 
		      new Vector3(0.963184f, 0.251881f, 0.0939874f ) , 
		      new Vector3(0.171426f, 0.945345f, 0.277373f ) , 
		      new Vector3(0.0580874f, 0.993877f, 0.0939874f ) , 
		      new Vector3(-0.846103f, 0.455163f, 0.277373f ) , 
		      new Vector3(-0.927284f, 0.362369f, 0.0939874f ) , 
		      new Vector3(-0.694346f, -0.664039f, 0.277373f ) , 
		      new Vector3(-0.63118f, -0.769921f, 0.0939874f ) , 
		      new Vector3(0.416974f, -0.865562f, 0.277373f ) , 
		      new Vector3(0.537193f, -0.838206f, 0.0939874f ) , 
		      new Vector3(0.95205f, -0.129092f, 0.277373f ) , 
		      new Vector3(0.963184f, -0.25188f, 0.0939874f ) , 
		      new Vector3(0.416974f, 0.865562f, 0.277373f ) , 
		      new Vector3(0.537193f, 0.838207f, 0.0939874f ) , 
		      new Vector3(-0.694346f, 0.664039f, 0.277373f ) , 
		      new Vector3(-0.63118f, 0.769921f, 0.0939874f ) , 
		      new Vector3(-0.846103f, -0.455163f, 0.277373f ) , 
		      new Vector3(-0.927284f, -0.36237f, 0.0939874f ) , 
		      new Vector3(0.171426f, -0.945345f, 0.277373f ) , 
		      new Vector3(0.0580875f, -0.993877f, 0.0939874f ) , 
		      new Vector3(0.289817f, 0.210565f, 0.933632f ) , 
		      new Vector3(0.502093f, 0.212399f, 0.838326f ) , 
		      new Vector3(0.357158f, 0.411884f, 0.838326f ) , 
		      new Vector3(0.69099f, 0.212688f, 0.690867f ) , 
		      new Vector3(0.569213f, 0.413557f, 0.71061f ) , 
		      new Vector3(0.415806f, 0.591446f, 0.690867f ) , 
		      new Vector3(-0.1107f, 0.340701f, 0.933632f ) , 
		      new Vector3(-0.0468481f, 0.543153f, 0.838326f ) , 
		      new Vector3(-0.281357f, 0.466957f, 0.838326f ) , 
		      new Vector3(0.0112493f, 0.722895f, 0.690867f ) , 
		      new Vector3(-0.21742f, 0.66915f, 0.71061f ) , 
		      new Vector3(-0.434008f, 0.578222f, 0.690867f ) , 
		      new Vector3(-0.358234f, 0.0f, 0.933632f ) , 
		      new Vector3(-0.531046f, 0.123288f, 0.838326f ) , 
		      new Vector3(-0.531046f, -0.123289f, 0.838326f ) , 
		      new Vector3(-0.684038f, 0.234085f, 0.690867f ) , 
		      new Vector3(-0.703586f, 0.0f, 0.71061f ) , 
		      new Vector3(-0.684038f, -0.234086f, 0.690867f ) , 
		      new Vector3(-0.1107f, -0.340701f, 0.933632f ) , 
		      new Vector3(-0.281357f, -0.466957f, 0.838326f ) , 
		      new Vector3(-0.046848f, -0.543153f, 0.838326f ) , 
		      new Vector3(-0.434008f, -0.578222f, 0.690867f ) , 
		      new Vector3(-0.21742f, -0.66915f, 0.71061f ) , 
		      new Vector3(0.0112494f, -0.722895f, 0.690867f ) , 
		      new Vector3(0.289817f, -0.210565f, 0.933632f ) , 
		      new Vector3(0.357158f, -0.411884f, 0.838326f ) , 
		      new Vector3(0.502093f, -0.212399f, 0.838326f ) , 
		      new Vector3(0.415806f, -0.591446f, 0.690867f ) , 
		      new Vector3(0.569213f, -0.413557f, 0.71061f ) , 
		      new Vector3(0.69099f, -0.212688f, 0.690867f ) , 
		      new Vector3(0.995273f, 0.0f, 0.0971187f ) , 
		      new Vector3(0.307556f, 0.946561f, 0.0971187f ) , 
		      new Vector3(-0.805193f, 0.585007f, 0.0971187f ) , 
		      new Vector3(-0.805193f, -0.585007f, 0.0971187f ) , 
		      new Vector3(0.307556f, -0.946561f, 0.0971187f ) , 
		      new Vector3(0.726285f, 0.68007f, 0.100072f ) , 
		      new Vector3(0.87122f, 0.480585f, 0.100072f ) , 
		      new Vector3(0.609811f, 0.732399f, 0.302857f ) , 
		      new Vector3(0.768762f, 0.558538f, 0.311512f ) , 
		      new Vector3(0.884995f, 0.353641f, 0.302857f ) , 
		      new Vector3(-0.422351f, 0.900892f, 0.100072f ) , 
		      new Vector3(-0.187842f, 0.977088f, 0.100072f ) , 
		      new Vector3(-0.508111f, 0.806288f, 0.302857f ) , 
		      new Vector3(-0.293641f, 0.903734f, 0.311512f ) , 
		      new Vector3(-0.062854f, 0.950961f, 0.302857f ) , 
		      new Vector3(-0.987312f, -0.123289f, 0.100072f ) , 
		      new Vector3(-0.987312f, 0.123288f, 0.100072f ) , 
		      new Vector3(-0.923841f, -0.234086f, 0.302857f ) , 
		      new Vector3(-0.950242f, 0.0f, 0.311512f ) , 
		      new Vector3(-0.923841f, 0.234085f, 0.302857f ) , 
		      new Vector3(-0.187842f, -0.977088f, 0.100072f ) , 
		      new Vector3(-0.422351f, -0.900892f, 0.100072f ) , 
		      new Vector3(-0.0628539f, -0.950961f, 0.302857f ) , 
		      new Vector3(-0.293641f, -0.903734f, 0.311512f ) , 
		      new Vector3(-0.508111f, -0.806288f, 0.302857f ) , 
		      new Vector3(0.87122f, -0.480585f, 0.100072f ) , 
		      new Vector3(0.726285f, -0.68007f, 0.100072f ) , 
		      new Vector3(0.884995f, -0.353641f, 0.302857f ) , 
		      new Vector3(0.768762f, -0.558538f, 0.311512f ) , 
		      new Vector3(0.609811f, -0.732399f, 0.302857f )  
		}; 
		#endregion

		#region AODirs_91
		public static readonly Vector3[] aoDirs_91 = new Vector3[91] {
		      new Vector3(0.0f, 0.0f, 1.0f ) , 
		      new Vector3(0.894685f, 0.0f, 0.446697f ) , 
		      new Vector3(0.276473f, 0.850896f, 0.446697f ) , 
		      new Vector3(-0.723816f, 0.525883f, 0.446697f ) , 
		      new Vector3(-0.723816f, -0.525883f, 0.446697f ) , 
		      new Vector3(0.276473f, -0.850896f, 0.446697f ) , 
		      new Vector3(0.287684f, 0.0f, 0.957725f ) , 
		      new Vector3(0.544623f, 0.0f, 0.838681f ) , 
		      new Vector3(0.749913f, 0.0f, 0.661537f ) , 
		      new Vector3(0.0888993f, 0.273604f, 0.957725f ) , 
		      new Vector3(0.168298f, 0.517967f, 0.838681f ) , 
		      new Vector3(0.231736f, 0.713209f, 0.661537f ) , 
		      new Vector3(-0.232742f, 0.169097f, 0.957725f ) , 
		      new Vector3(-0.440609f, 0.320121f, 0.838681f ) , 
		      new Vector3(-0.606692f, 0.440788f, 0.661537f ) , 
		      new Vector3(-0.232742f, -0.169097f, 0.957725f ) , 
		      new Vector3(-0.440609f, -0.320121f, 0.838681f ) , 
		      new Vector3(-0.606692f, -0.440788f, 0.661537f ) , 
		      new Vector3(0.0888994f, -0.273604f, 0.957725f ) , 
		      new Vector3(0.168298f, -0.517967f, 0.838681f ) , 
		      new Vector3(0.231736f, -0.713209f, 0.661537f ) , 
		      new Vector3(0.825322f, 0.260746f, 0.500854f ) , 
		      new Vector3(0.691266f, 0.502234f, 0.519531f ) , 
		      new Vector3(0.503023f, 0.704353f, 0.500854f ) , 
		      new Vector3(0.00705452f, 0.865503f, 0.500854f ) , 
		      new Vector3(-0.26404f, 0.812632f, 0.519531f ) , 
		      new Vector3(-0.514437f, 0.69606f, 0.500854f ) , 
		      new Vector3(-0.820962f, 0.274164f, 0.500854f ) , 
		      new Vector3(-0.854452f, 0.0f, 0.519531f ) , 
		      new Vector3(-0.820962f, -0.274164f, 0.500854f ) , 
		      new Vector3(-0.514437f, -0.69606f, 0.500854f ) , 
		      new Vector3(-0.26404f, -0.812632f, 0.519531f ) , 
		      new Vector3(0.00705465f, -0.865503f, 0.500854f ) , 
		      new Vector3(0.503023f, -0.704353f, 0.500854f ) , 
		      new Vector3(0.691266f, -0.502234f, 0.519531f ) , 
		      new Vector3(0.825322f, -0.260746f, 0.500854f ) , 
		      new Vector3(0.954427f, 0.159814f, 0.252049f ) , 
		      new Vector3(0.950131f, 0.308716f, 0.0441132f ) , 
		      new Vector3(0.142942f, 0.957099f, 0.252049f ) , 
		      new Vector3(0.0f, 0.999027f, 0.0441132f ) , 
		      new Vector3(-0.866084f, 0.431706f, 0.252049f ) , 
		      new Vector3(-0.950131f, 0.308716f, 0.0441132f ) , 
		      new Vector3(-0.678211f, -0.69029f, 0.252049f ) , 
		      new Vector3(-0.587213f, -0.808229f, 0.0441132f ) , 
		      new Vector3(0.446926f, -0.858329f, 0.252049f ) , 
		      new Vector3(0.587213f, -0.808229f, 0.0441132f ) , 
		      new Vector3(0.954427f, -0.159814f, 0.252049f ) , 
		      new Vector3(0.950131f, -0.308716f, 0.0441132f ) , 
		      new Vector3(0.446926f, 0.858329f, 0.252049f ) , 
		      new Vector3(0.587213f, 0.80823f, 0.0441132f ) , 
		      new Vector3(-0.678211f, 0.69029f, 0.252049f ) , 
		      new Vector3(-0.587213f, 0.808229f, 0.0441132f ) , 
		      new Vector3(-0.866084f, -0.431706f, 0.252049f ) , 
		      new Vector3(-0.950131f, -0.308716f, 0.0441132f ) , 
		      new Vector3(0.142942f, -0.957099f, 0.252049f ) , 
		      new Vector3(0.0f, -0.999027f, 0.0441132f ) , 
		      new Vector3(0.377049f, 0.273942f, 0.884754f ) , 
		      new Vector3(0.631774f, 0.270635f, 0.726373f ) , 
		      new Vector3(0.452618f, 0.517222f, 0.726373f ) , 
		      new Vector3(-0.14402f, 0.443248f, 0.884754f ) , 
		      new Vector3(-0.0621602f, 0.684484f, 0.726373f ) , 
		      new Vector3(-0.352041f, 0.590296f, 0.726373f ) , 
		      new Vector3(-0.466058f, 0.0f, 0.884754f ) , 
		      new Vector3(-0.670191f, 0.152399f, 0.726373f ) , 
		      new Vector3(-0.670191f, -0.152399f, 0.726373f ) , 
		      new Vector3(-0.14402f, -0.443248f, 0.884754f ) , 
		      new Vector3(-0.352041f, -0.590296f, 0.726373f ) , 
		      new Vector3(-0.0621601f, -0.684484f, 0.726373f ) , 
		      new Vector3(0.377049f, -0.273942f, 0.884754f ) , 
		      new Vector3(0.452618f, -0.517222f, 0.726373f ) , 
		      new Vector3(0.631774f, -0.270635f, 0.726373f ) , 
		      new Vector3(0.999027f, 0.0f, 0.0441132f ) , 
		      new Vector3(0.308716f, 0.950131f, 0.0441132f ) , 
		      new Vector3(-0.80823f, 0.587213f, 0.0441132f ) , 
		      new Vector3(-0.808229f, -0.587213f, 0.0441132f ) , 
		      new Vector3(0.308716f, -0.950131f, 0.0441132f ) , 
		      new Vector3(0.808229f, 0.587213f, 0.0441132f ) , 
		      new Vector3(0.681945f, 0.678791f, 0.272385f ) , 
		      new Vector3(0.856301f, 0.43881f, 0.272385f ) , 
		      new Vector3(-0.308716f, 0.950131f, 0.0441132f ) , 
		      new Vector3(-0.434836f, 0.858326f, 0.272385f ) , 
		      new Vector3(-0.152722f, 0.949991f, 0.272385f ) , 
		      new Vector3(-0.999027f, 0.0f, 0.0441132f ) , 
		      new Vector3(-0.950688f, -0.148317f, 0.272385f ) , 
		      new Vector3(-0.950688f, 0.148316f, 0.272385f ) , 
		      new Vector3(-0.308716f, -0.950131f, 0.0441132f ) , 
		      new Vector3(-0.152722f, -0.949991f, 0.272385f ) , 
		      new Vector3(-0.434836f, -0.858326f, 0.272385f ) , 
		      new Vector3(0.80823f, -0.587213f, 0.0441132f ) , 
		      new Vector3(0.856301f, -0.43881f, 0.272385f ) , 
		      new Vector3(0.681945f, -0.678791f, 0.272385f )  
		};
		#endregion 

		#region AODirs_46
		public static readonly Vector3[] aoDirs_46 = new Vector3[46] {
		      new Vector3(0.0f, 0.0f, 1.0f ) , 
		      new Vector3(0.927182f, 0.0f, 0.374611f ) , 
		      new Vector3(0.286515f, 0.881803f, 0.374611f ) , 
		      new Vector3(-0.750106f, 0.544984f, 0.374611f ) , 
		      new Vector3(-0.750106f, -0.544984f, 0.374611f ) , 
		      new Vector3(0.286515f, -0.881803f, 0.374611f ) , 
		      new Vector3(0.379499f, 0.0f, 0.925192f ) , 
		      new Vector3(0.706754f, 0.0f, 0.70746f ) , 
		      new Vector3(0.117272f, 0.360925f, 0.925192f ) , 
		      new Vector3(0.218399f, 0.672163f, 0.70746f ) , 
		      new Vector3(-0.307021f, 0.223064f, 0.925192f ) , 
		      new Vector3(-0.571776f, 0.415419f, 0.70746f ) , 
		      new Vector3(-0.307021f, -0.223064f, 0.925192f ) , 
		      new Vector3(-0.571776f, -0.415419f, 0.70746f ) , 
		      new Vector3(0.117272f, -0.360925f, 0.925192f ) , 
		      new Vector3(0.218399f, -0.672163f, 0.70746f ) , 
		      new Vector3(0.816551f, 0.357156f, 0.453524f ) , 
		      new Vector3(0.592004f, 0.666218f, 0.453524f ) , 
		      new Vector3(-0.0873479f, 0.886953f, 0.453524f ) , 
		      new Vector3(-0.450672f, 0.768902f, 0.453524f ) , 
		      new Vector3(-0.870535f, 0.191011f, 0.453524f ) , 
		      new Vector3(-0.870535f, -0.191011f, 0.453524f ) , 
		      new Vector3(-0.450672f, -0.768902f, 0.453524f ) , 
		      new Vector3(-0.0873477f, -0.886953f, 0.453524f ) , 
		      new Vector3(0.592004f, -0.666218f, 0.453524f ) , 
		      new Vector3(0.816551f, -0.357156f, 0.453524f ) , 
		      new Vector3(0.975405f, 0.21438f, 0.0512521f ) , 
		      new Vector3(0.0975293f, 0.993912f, 0.0512521f ) , 
		      new Vector3(-0.915128f, 0.399892f, 0.0512521f ) , 
		      new Vector3(-0.66311f, -0.746765f, 0.0512521f ) , 
		      new Vector3(0.505304f, -0.861418f, 0.0512521f ) , 
		      new Vector3(0.975405f, -0.21438f, 0.0512521f ) , 
		      new Vector3(0.505304f, 0.861418f, 0.0512521f ) , 
		      new Vector3(-0.66311f, 0.746765f, 0.0512521f ) , 
		      new Vector3(-0.915128f, -0.399892f, 0.0512521f ) , 
		      new Vector3(0.0975294f, -0.993912f, 0.0512521f ) , 
		      new Vector3(0.504242f, 0.366354f, 0.782f ) , 
		      new Vector3(-0.192603f, 0.592772f, 0.782001f ) , 
		      new Vector3(-0.623278f, 0.0f, 0.782f ) , 
		      new Vector3(-0.192603f, -0.592772f, 0.782001f ) , 
		      new Vector3(0.504242f, -0.366353f, 0.782f ) , 
		      new Vector3(0.807237f, 0.586492f, 0.0662973f ) , 
		      new Vector3(-0.308337f, 0.948964f, 0.0662973f ) , 
		      new Vector3(-0.9978f, 0.0f, 0.0662973f ) , 
		      new Vector3(-0.308337f, -0.948964f, 0.0662973f ) , 
		      new Vector3(0.807237f, -0.586492f, 0.0662973f )  
		}; 
		#endregion 

		#region AODirs_26
		public static readonly Vector3[] aoDirs_26 = new Vector3[26] {
	      new Vector3(0.0f, 0.0f, 1.0f ) , 
	      new Vector3(0.887643f, 0.0f, 0.460532f ) , 
	      new Vector3(0.274297f, 0.844199f, 0.460532f ) , 
	      new Vector3(-0.718118f, 0.521743f, 0.460532f ) , 
	      new Vector3(-0.718118f, -0.521744f, 0.460532f ) , 
	      new Vector3(0.274297f, -0.844199f, 0.460532f ) , 
	      new Vector3(0.546f, 0.0f, 0.837785f ) , 
	      new Vector3(0.168723f, 0.519277f, 0.837785f ) , 
	      new Vector3(-0.441724f, 0.320931f, 0.837785f ) , 
	      new Vector3(-0.441723f, -0.320931f, 0.837785f ) , 
	      new Vector3(0.168723f, -0.519277f, 0.837785f ) , 
	      new Vector3(0.686379f, 0.498683f, 0.529338f ) , 
	      new Vector3(-0.262173f, 0.806887f, 0.529338f ) , 
	      new Vector3(-0.848411f, 0.0f, 0.529338f ) , 
	      new Vector3(-0.262173f, -0.806887f, 0.529338f ) , 
	      new Vector3(0.686379f, -0.498683f, 0.529338f ) , 
	      new Vector3(0.9477f, 0.307926f, 0.0839444f ) , 
	      new Vector3(0.0f, 0.99647f, 0.0839444f ) , 
	      new Vector3(-0.9477f, 0.307926f, 0.0839444f ) , 
	      new Vector3(-0.585711f, -0.806161f, 0.0839444f ) , 
	      new Vector3(0.585711f, -0.806161f, 0.0839444f ) , 
	      new Vector3(0.9477f, -0.307926f, 0.0839444f ) , 
	      new Vector3(0.585711f, 0.806162f, 0.0839444f ) , 
	      new Vector3(-0.585711f, 0.806161f, 0.0839444f ) , 
	      new Vector3(-0.9477f, -0.307926f, 0.0839444f ) , 
	      new Vector3(0.0f, -0.99647f, 0.0839444f )  
	};
	#endregion 

		#region AODirs_7
		public static readonly Vector3[] aoDirs_7 = new Vector3[7] {
	      new Vector3(0.0f, 0.0f, 1.0f ) , 
	      new Vector3(0.80696f, -0.489651f, 0.330237f ) , 
	      new Vector3(0.830284f, 0.445317f, 0.335142f ) , 
	      new Vector3(-0.823841f, 0.453385f, 0.340188f ) , 
	      new Vector3(-0.806455f, -0.490844f, 0.3297f ) , 
	      new Vector3(-0.00196244f, -0.935204f, 0.354104f ) , 
	      new Vector3(0.00309103f, 0.932336f, 0.36158f )  
		}; 
		#endregion
	
 		public void ApplyAO(Mesh m, ObjData obj){
	
			bool hasMC_AtBegin = GetComponent<MeshCollider>();
			MeshCollider mc = null;

			if(!hasMC_AtBegin){
				mc = gameObject.AddComponent<MeshCollider>();
			} else {
				mc = GetComponent<MeshCollider>();
			}

			RaycastHit hit;
			Matrix4x4 objTM = transform.localToWorldMatrix;
 
			Vector3[] samples = aoDirs_7;
			if(AOQuality == AOQualityEnum.Low){
				samples = aoDirs_26;
			} else if(AOQuality == AOQualityEnum.Medium){
				samples = aoDirs_46;
			} else	if(AOQuality == AOQualityEnum.Hight){
				samples = aoDirs_91;
			} else	if(AOQuality == AOQualityEnum.Ultra){
				samples = aoDirs_126;
			}

			mc.sharedMesh = m;

			//POLY PASS
			for(int p = 0; p<obj.AllPolygons.Count; p++){
				Polygon poly = obj.AllPolygons[p];
				poly.AO = 0;
				Vector3 origin = Vector3.zero;
				float mult = 1f/poly.corners.Count;
				for(int c = 0; c<poly.corners.Count; c++){
					origin += poly.corners[c].Position.Value * mult;
	 			}	
				Vector3 polyNormal = objTM.MultiplyVector( poly.Normal ); 
				Vector3 polyOrigin = objTM.MultiplyPoint3x4(origin ) + polyNormal * AOOffset; 
	 

				Matrix4x4 normalTM = Matrix4x4.TRS(Vector3.zero, Quaternion.LookRotation( polyNormal, Vector3.up ), Vector3.one );
	
				 
				for(int i = 0; i<samples.Length; i++){
					Vector3 dir = normalTM.MultiplyVector( samples[i]);
					Ray sampleRay = new Ray(polyOrigin, dir);
					if(mc.Raycast(sampleRay, out hit, AORadius )){
						poly.AO += (1f-(hit.distance/AORadius))/ samples.Length  ; 
	 				}
				}
			} 

			//INVERT MC
			Mesh inverted = Instantiate( m ) as Mesh;
			int[] tris = inverted.triangles;
 
			for(int t = 0; t<tris.Length; t+=3){
				int t0 = tris[t+2];
				int t1 = tris[t+1];
				int t2 = tris[t ];
				tris[t] = t0;
				tris[t+1] = t1;
				tris[t+2] = t2;
			}

			inverted.triangles = tris;
			mc.sharedMesh = inverted;


			//POLY PASS
			for(int p = 0; p<obj.AllPolygons.Count; p++){
				Polygon poly = obj.AllPolygons[p];

				float innerAO = 0;
				Vector3 origin = Vector3.zero;
				float mult = 1f/poly.corners.Count;
				for(int c = 0; c<poly.corners.Count; c++){
					origin += poly.corners[c].Position.Value * mult;
	 			}	
				Vector3 polyNormal = objTM.MultiplyVector( poly.Normal ); 
				Vector3 polyOrigin = objTM.MultiplyPoint3x4(origin ) + polyNormal * AOOffset ; 
				Matrix4x4 normalTM = Matrix4x4.TRS(Vector3.zero, Quaternion.LookRotation( polyNormal, Vector3.up ), Vector3.one );
				for(int i = 0; i<samples.Length; i++){
					Vector3 dir = normalTM.MultiplyVector( samples[i]);
					Ray sampleRay = new Ray(polyOrigin, dir);
					if(mc.Raycast(sampleRay, out hit, AORadius )){
						innerAO += (1f-(hit.distance/AORadius))/ samples.Length  ; 
	 				}
				}
				poly.AO = Mathf.Max(poly.AO, innerAO );
			} 
 


			for(int n = 0; n<obj.Normals.Count; n++){
				NormalVertex nv = obj.Normals[n];
				nv.AO = 0;
 				float mult = 1f/ nv.AdjacentPolygons.Count;
				for(int a = 0; a<nv.AdjacentPolygons.Count; a++){
					Polygon p = nv.AdjacentPolygons[a];
					nv.AO  += p.AO*mult;
				}
			}

			for(int i = 0; i<SmoothAOIterations; i++){

		 		for(int p = 0; p<obj.AllPolygons.Count; p++){
		 			float ao = 0; 
		 			Polygon poly = obj.AllPolygons[p];
					float mult = 1f/poly.corners.Count;
		 			for(int c = 0; c<poly.corners.Count; c++){
						ao += poly.corners[c].Normal.AO * mult;
		 			}
		 			poly.AO = ao;
		 		}

				for(int n = 0; n<obj.Normals.Count; n++){
					NormalVertex nv = obj.Normals[n];
					nv.AO = 0;
	 				float mult = 1f/ nv.AdjacentPolygons.Count;
					for(int a = 0; a<nv.AdjacentPolygons.Count; a++){
						Polygon p = nv.AdjacentPolygons[a];
						nv.AO  += p.AO*mult;
					}
				}
			}



			Color[] colors = new Color[m.vertexCount];
 
			for(int v = 0; v<obj.UnityVertices.Count; v++){
				colors[v] = new Color( 0,0,0,   obj.UnityVertices[v].Normal.AO  * AOIntensity  );
			}
			m.colors = colors;
 
		}

 

 
	}
}
