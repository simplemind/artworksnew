﻿using UnityEngine;
using System.Collections;

public class DetectDoubleTap : MonoBehaviour {

	float touchDuration;
	int touchCount;
	Touch touch;
    CaptureAndSave snapShot;

    public GameObject[] interfaceArray;
    private bool[] interfaceVisibility;

    void Start()
    {
        snapShot = GameObject.FindObjectOfType<CaptureAndSave>();
    }

    void OnEnable()
    {
        CaptureAndSaveEventListener.onError += OnError;
        CaptureAndSaveEventListener.onSuccess += OnSuccess;
    }

    void OnDisable()
    {
        CaptureAndSaveEventListener.onError += OnError;
        CaptureAndSaveEventListener.onSuccess += OnSuccess;
    }

    void OnError(string error)
    {
        Debug.Log("Error : " + error);
    }

    void OnSuccess(string msg)
    {
        Debug.Log("Success : " + msg);
    }

    void Update() {
		if (Input.touchCount > 0) { //if there is any touch
			touchDuration += Time.deltaTime;
			touch = Input.GetTouch (0);
			if (touch.phase != TouchPhase.Ended) {
				if (touchDuration > 2.0f) {//making sure it only check the touch once && it was a short touch/tap and not a dragging.
				//StartCoroutine ("singleOrDouble");
					TapAndHold();
					touchDuration = 0.0f;
				}
			} else {
				touchDuration = 0.0f;
			}
		} else {
			touchDuration = 0.0f;
		}
	}
	
	IEnumerator singleOrDouble(){
		touchCount ++;
		yield return new WaitForSeconds(0.3f);
		if (touchCount == 1) {
			Debug.Log ("Single");
			touchCount = 0;
		} else {
			if (touchCount == 2) {
				//this coroutine has been called twice. We should stop the next one here otherwise we get two double tap
				StopCoroutine ("singleOrDouble");
				Debug.Log ("Double");
				this.GetComponent<TakeScreenshots>().takeScreenshot = true;
			}
			touchCount = 0;
		}
	}

	public void TapAndHold(){
        StoreInterfaceVisibility(out interfaceVisibility);
        HideInterfaceVisibility();
        StartCoroutine(ScreenshotTransition());
    }

    void HideInterfaceVisibility ()
    {
        foreach (GameObject interfaceObj in interfaceArray)
        {
            interfaceObj.SetActive(false);
        }
    }

    void StoreInterfaceVisibility(out bool[] status)
    {
        status = new bool[interfaceArray.Length];
        for (int i=0; i < interfaceArray.Length; i++)
        {
            status[i] = interfaceArray[i].activeInHierarchy;
        }
    }

    void RestoreInterfaceVisibility(bool[] status)
    {
        for (int i = 0; i < interfaceArray.Length; i++)
        {
            interfaceArray[i].SetActive(status[i]);
        }
    }

    IEnumerator ScreenshotTransition()
    {
#if !UNITY_ANDROID || UNITY_EDITOR
        snapShot.CaptureAndSaveToAlbum();
#endif
        yield return new WaitForEndOfFrame();
        this.GetComponent<TakeScreenshots>().takeScreenshot = true;
        yield return new WaitForEndOfFrame();

        RestoreInterfaceVisibility(interfaceVisibility);
    }
}
