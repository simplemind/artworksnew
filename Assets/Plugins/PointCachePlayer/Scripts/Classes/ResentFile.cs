﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

namespace pc2unity_20{
	public enum ResentFileState{
		EmptyPath,
		NotExist,
		ErrorRead,
		ReadyForImport
	}

	[System.Serializable]
	public class ResentFile {
		public string DirectoryPath;
		public string FileModifiedDate;
		public ResentFileState state = ResentFileState.EmptyPath;
		public string StatisticString;


		[SerializeField]
		string _filePath;
		public string FilePath{
			get{
				return _filePath;
			}

			set{
				_filePath = value;
				 
				if(_filePath == null || _filePath.Length == 0){
					state = ResentFileState.EmptyPath;
					StatisticString = "";
					FileModifiedDate = "";
					return;
				} 

				if( System.IO.File.Exists( _filePath )) {
					FileInfo fi = new FileInfo( _filePath );
					string newDate = fi.LastWriteTime.ToString();
					if( newDate !=  FileModifiedDate ){
						if(FillStatistic( _filePath )){
							FileModifiedDate = newDate;
							DirectoryPath = fi.DirectoryName;
						} else {
							state = ResentFileState.ErrorRead;
							StatisticString = "";
							FileModifiedDate = "";
							return;
						}
					}
					state = ResentFileState.ReadyForImport;  
					return;
				} else {
					state = ResentFileState.NotExist;
					StatisticString = "";
					FileModifiedDate = "";
					return;
				}
			}
		}

		public string ButtonName{
			get{
				if (state == ResentFileState.ReadyForImport){
					return FilePath;
				} 
				if(state == ResentFileState.ErrorRead){
					return "Error read: "+FilePath;
				}
 				if(state == ResentFileState.NotExist){
					return "Not found:"+FilePath ;
				}
				return "Not selected" ;
			}
		}
 

 		public bool IsValidDirectory{
 			get{
				return DirectoryPath!=null && DirectoryPath.Length>0;
 			}
 		}

 		public virtual bool FillStatistic( string filePath ){
 			return false;
 		}

	}
 
}
