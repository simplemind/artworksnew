﻿using UnityEngine;
using System.Collections;
using System.IO;
using UnityEngine.UI;

public class TakeScreenshotsOld : MonoBehaviour 
{
	bool creatingFile = false;
	string fileName = "Cruise-"; //start of file name; later the current time is added for unique identifier
	string crossoverFileName, destination, root, origin;
	string pathToGo = "/CaptainCruise/"; //Could change this name to any other; is the folder in the root directory of the (emulated) sdcard where screens are saved
	[HideInInspector] public bool takeScreenshot = false;
	public GameObject cameraButton;
	public GameObject screenImage;

	void Start() {
		root = "";
#if UNITY_ANDROID
		AndroidJavaClass Environment = new AndroidJavaClass("android.os.Environment");
		using (AndroidJavaObject externalStorageDirectory = Environment.CallStatic<AndroidJavaObject>("getExternalStorageDirectory"))
			root = externalStorageDirectory.Call<string>("getAbsolutePath"); // could be anything
		string dcimDirectory = Environment.GetStatic<string>("DIRECTORY_DCIM");
#endif
		crossoverFileName = root + "/" + dcimDirectory + pathToGo;
		Directory.CreateDirectory (crossoverFileName);
		if (Directory.Exists (crossoverFileName))
			Debug.Log (crossoverFileName);
		else
			Debug.Log ("NoPathToGo");
	}


	public void TakeScreenshot()
	{
		takeScreenshot = true;

	}


	void LateUpdate()
	{
		if (takeScreenshot) 
		{
						//callTextWindow ("HELP");
			string screenFileName = fileName + System.DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss") + ".png";
			StartCoroutine(WaitOneFrame(screenFileName));
			Debug.Log ("ScreenShot Taken!");
			creatingFile = true;
			if (creatingFile) {
				origin = Path.Combine(Application.persistentDataPath, screenFileName);
				Debug.Log("origin: " + origin);
				destination = crossoverFileName + screenFileName;
//				callTextWindow (origin + "__result: " + destination);
				Debug.Log("destination: " + destination);

			}
			takeScreenshot = false;
			if (cameraButton != null) cameraButton.GetComponent<Button> ().interactable = false;

		}
	}

	// Update is called once per frame
	void Update () {
		if (creatingFile) {
			//Debug.Log("waiting");
			if (File.Exists(origin)) {
				//callTextWindow ("SCREENSHOT TAKEN!");
				Debug.Log("FileExists! " + origin);
				System.IO.File.Move(origin, destination);
				if (System.IO.File.Exists(destination)) {
					Debug.Log("DestinationFileExists! " + destination);
				}
				creatingFile = false;

				#if UNITY_ANDROID
				using (AndroidJavaClass jcUnityPlayer = new AndroidJavaClass ("com.unity3d.player.UnityPlayer"))
				using (AndroidJavaObject joActivity = jcUnityPlayer.GetStatic<AndroidJavaObject> ("currentActivity"))
				using (AndroidJavaObject joContext = joActivity.Call<AndroidJavaObject> ("getApplicationContext"))
				using (AndroidJavaClass jcMediaScannerConnection = new AndroidJavaClass ("android.media.MediaScannerConnection"))
				using (AndroidJavaClass jcEnvironment = new AndroidJavaClass ("android.os.Environment")) {
					jcMediaScannerConnection.CallStatic("scanFile", joContext, new string[] { destination }, null, null);
				}
				#endif

				if (cameraButton != null) cameraButton.GetComponent<Button> ().interactable = true;
			}
		}
	}

	IEnumerator WaitOneFrame (string screenFileName) {
		yield return null;
		Canvas uiOff = GameObject.Find ("Canvas").GetComponent<Canvas> ();
		uiOff.enabled = false;
		yield return new WaitForEndOfFrame ();
		if (Application.platform == RuntimePlatform.WindowsEditor) {
			ScreenCapture.CaptureScreenshot (origin);
		} else {
			ScreenCapture.CaptureScreenshot (screenFileName);
		}
		if (screenImage != null) {
			Debug.Log("Making texture");
			Texture2D texture = new Texture2D (Screen.width, Screen.height, TextureFormat.RGB24, false);
			texture.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0, false);
			texture.Apply();
			Sprite screenSprite = Sprite.Create (texture, new Rect (0, 0, Screen.width, Screen.height), new Vector2 (0, 0));
			screenImage.GetComponent<Image> ().enabled = true;
			screenImage.GetComponent<Image> ().sprite = screenSprite;
			screenImage.GetComponent<Animator>().Play("DownAndOut");
			StartCoroutine(killImage(screenImage.GetComponent<Image>()));
		}
		uiOff.enabled = true;

/*		//REFRESHING THE ANDROID PHONE PHOTO GALLERY IS BEGUN
#if UNITY_ANDROID
		AndroidJavaClass classPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject objActivity = classPlayer.GetStatic<AndroidJavaObject>("currentActivity");
		AndroidJavaObject objMediaScanner = classPlayer.GetStatic<AndroidJavaObject>("MediaScannerConnection");
		AndroidJavaClass classUri = new AndroidJavaClass("android.net.Uri");        
		Debug.Log (root);
		Debug.Log (destination);
		AndroidJavaObject objIntent = new AndroidJavaObject("android.content.Intent", new object[2]{"android.intent.action.MEDIA_SCANNER_SCAN_FILE", classUri.CallStatic<AndroidJavaObject>("parse", "file://" + destination)});
		objActivity.Call ("sendBroadcast", objIntent);
#endif
		//REFRESHING THE ANDROID PHONE PHOTO GALLERY IS COMPLETE
		*/
	}

	IEnumerator killImage(Image imageToKill) {
		yield return new WaitForSeconds(1.6f);

		imageToKill.enabled = false;
	}

/*
	void callTextWindow (string textToCall) {
		GameObject screenshotText = GameObject.Find("ScreenshotText");
		Text message = screenshotText.GetComponent <Text> ();
		message.fontSize = 12;
		message.text = textToCall;
		FadeOutBehaviour fob = screenshotText.GetComponent <FadeOutBehaviour> ();
		fob.screenshotTaken = true;
	}
*/

}

