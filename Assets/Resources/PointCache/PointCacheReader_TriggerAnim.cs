﻿//
//POINT CACHE READER
//
//Created By: Dejan Omasta
//email: list3ner@gmail.com
//
//=================================================
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class PointCacheReader_TriggerAnim : MonoBehaviour
{
    //
    //POINT CACHE Vs
    //
    [System.Serializable]
    public struct pointCacheAnimationData
    {
        public string fileName;
        public int startFrame;
    }

    public pointCacheAnimationData filePush, filePull;

    struct PointCacheFile
    {
        public char[] signature;
        public int fileVersion;
        public int numPoints;
        public float startFrame;
        public float sampleRate;
        public int numSamples;
        public List<Vector3> vertexCoords;
    }

    PointCacheFile pcPush, pcPull;
    bool fileParsed = false;
    //
    //VERTEX POS Vs
    //
    public float fps = 24.0f;
    Mesh mesh;
    Vector3[] vertices;
    int counter = 0;
    int curFrame = 0;
    public int startFrame = 0;
    float timeMeasure = 0.0f;

    //Playback control
    bool playPullEnded = false;
    bool playPushEnded = false;
    bool playPullStart = false;
    bool playPushStart = false;
    bool pulled = false;

    // Use this for initialization
    void Start()
    {
        pcPull = new PointCacheFile();
        ParsePCFile(out pcPull, filePull.fileName);
        pcPush = new PointCacheFile();
        ParsePCFile(out pcPush, filePush.fileName);
        timeMeasure = 1 / fps;
        mesh = GetComponent<MeshFilter>().mesh;
        vertices = mesh.vertices;
        curFrame = startFrame;
        counter = vertices.Length * curFrame;
    }

    void Update()
    {
        if (playPullEnded && IsInvoking("PlayPullToEnd"))
        {
            CancelInvoke("PlayPullToEnd");
            playPullEnded = false;
            pulled = true;
        }
        if (playPushEnded && IsInvoking("PlayPushToEnd"))
        {
            CancelInvoke("PlayPushToEnd");
            playPushEnded = false;
            pulled = false;
        }
        if (!IsInvoking() && fileParsed)
        {
            if (playPullStart)
            {
                if (!pulled)
                {
                    InvokeRepeating("PlayPullToEnd", 0.01f, timeMeasure);
                }
                playPullStart = false;
            }
            else if (playPushStart)
            {
                if (pulled)
                {
                    InvokeRepeating("PlayPushToEnd", 0.01f, timeMeasure);
                }
                playPushStart = false;
            }
        }
    }

    void OnTriggerEnter(Collider col)
    {
        playPushStart = false;
        playPullStart = true;
        Debug.Log("Pulling Curtains");
    }

    void OnTriggerExit(Collider col)
    {
        playPullStart = false;
        playPushStart = true;
        Debug.Log("Pushing Curtains");
    }

    void PlayLoop(PointCacheFile pcFile)
    {
        for (int x = 0; x < vertices.Length; x++)
        {
            if (counter < pcFile.vertexCoords.Count)
            {
                vertices[x] = pcFile.vertexCoords[counter];
            }
            counter++;
        }
        mesh.vertices = vertices;
        mesh.RecalculateBounds();
        mesh.RecalculateNormals();
        curFrame++;
        if (curFrame == pcFile.numSamples)
        {
            curFrame = 0;
            counter = 0;
        }
    }

    void PlayPullToEnd()
    {
        for (int x = 0; x < vertices.Length; x++)
        {
            if (counter < pcPull.vertexCoords.Count)
            {
                vertices[x] = pcPull.vertexCoords[counter];
            }
            counter++;
        }
        mesh.vertices = vertices;
        mesh.RecalculateBounds();
        mesh.RecalculateNormals();
        curFrame++;
        if (curFrame == pcPull.numSamples)
        {
            playPullEnded = true;
            curFrame = 0;
            counter = 0;
        }
    }

    void PlayPushToEnd()
    {
        for (int x = 0; x < vertices.Length; x++)
        {
            if (counter < pcPush.vertexCoords.Count)
            {
                vertices[x] = pcPush.vertexCoords[counter];
            }
            counter++;
        }
        mesh.vertices = vertices;
        mesh.RecalculateBounds();
        mesh.RecalculateNormals();
        curFrame++;
        if (curFrame == pcPush.numSamples)
        {
            playPushEnded = true;
            curFrame = 0;
            counter = 0;
        }
    }

    void ParsePCFile(out PointCacheFile pcFile, string fileName)
    {
        TextAsset asset = Resources.Load("PointCache/" + fileName) as TextAsset;
        Stream s = new MemoryStream(asset.bytes);
        //FileStream fs = new FileStream(filePath, FileMode.Open);
        BinaryReader binReader = new BinaryReader(s);
        //
        //SIGNATURE
        //
        pcFile.signature = new char[12];
        pcFile.signature = binReader.ReadChars(12);
        //
        //FILE VERSION
        //
        pcFile.fileVersion = binReader.ReadInt32();
        //
        //NUMBER OF POINTS
        //
        pcFile.numPoints = binReader.ReadInt32();
        //
        //START FRAME
        //
        pcFile.startFrame = binReader.ReadSingle();
        //
        //SAMPLE RATE
        //
        pcFile.sampleRate = binReader.ReadSingle();
        //
        //NUMBER OF SAMPLES
        //
        pcFile.numSamples = binReader.ReadInt32();
        //
        //GET VERTEX COORDS
        //
        pcFile.vertexCoords = new List<Vector3>();
        for (int i = 0; i < pcFile.numSamples; i++)
        {
            for (int x = 0; x < pcFile.numPoints; x++)
            {
                Vector3 vPos = new Vector3();
                try
                {
                    vPos.x = -binReader.ReadSingle() * .010f;
                    vPos.y = binReader.ReadSingle() * .010f;
                    vPos.z = binReader.ReadSingle() * .010f;
                    pcFile.vertexCoords.Add(vPos);
                }
                catch (EndOfStreamException e)
                {
                    Debug.Log(e.GetType().Name);
                }
            }
        }

        fileParsed = true;
        s.Close();

        // Debug Log
        Debug.Log(fileName + " " + pcFile.numPoints + " " + pcFile.vertexCoords.Count);
    }
}
