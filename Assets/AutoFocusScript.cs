﻿using UnityEngine;
using System.Collections;
using System;
using Vuforia;
 
public class AutoFocusScript : MonoBehaviour 
{
    private bool mVuforiaStarted = false;
 
    // Use this for initialization
    void Start ()
    {
        VuforiaARController vuf = VuforiaARController.Instance;
        vuf.RegisterVuforiaStartedCallback(OnVuforiaStarted);
    }
 
    private void OnVuforiaStarted()
    {
        Debug.Log(" OnVuforiaStarted ");
        mVuforiaStarted = true;
        SetAutofocus();
    }
 
    void OnApplicationPause(bool pause)
    {
        if (!pause)
        {
            // App resumed
            Debug.Log("App resumed");
            if (mVuforiaStarted)
            {
                // App resumed, and Vuforia already started previously
                SetAutofocus();
            }
        }
    }
 
    private void SetAutofocus()
    {
        CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
    }
}
